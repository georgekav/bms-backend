__author__ = 'Georgios Lilis'
import logging
import sys
import platform
from time import sleep
from abc import ABCMeta, abstractmethod

try:
    import ftd2xx
except (ImportError, OSError): #If thereis no driver. UART should use pyserial
    pass
import serial
import select


class UartApiError(Exception):
    """An exception to handle uart api exemption"""
    pass

class UartTimeoutError(Exception):
    """An exception to handle uart Timeout return during read or write"""
    pass

class UartInterface(object):
    """Abstract class for all the availiable uart communications"""
    __metaclass__ = ABCMeta

    @abstractmethod
    def connect(self):
        return

    @abstractmethod
    def disconnect(self):
        return

    @abstractmethod
    def read(self, numofbytes):
        return

    @abstractmethod
    def write(self, bytestowrite):
        return

    @abstractmethod
    def flush(self):
        return

    @abstractmethod
    def get_waiting(self):
        return

    @abstractmethod
    def block_and_wait_byte(self, waittime):
        return

class FtdiInterface(UartInterface):
    def __init__(self, usbconf):
        if platform.system() == 'Linux':
            logging.error('FtdiInterface d2xx driver not supported in linux')
            sys.exit()
        elif platform.system() == 'Windows':
            import win32event
            # CreateEvent(security_attributes, bManualReset , bInitialState , objectName )
            self._mywin32event = win32event.CreateEvent(None, True, False, '')
        self.device = None
        self.usbconf = usbconf
        self.api_error_counter = 0

    def connect(self):
        usbconf = self.usbconf
        try:
            dev = ftd2xx.openEx(usbconf.SerialDevice)
            dev.setFlowControl(flowcontrol=usbconf.FlowControl)
            dev.setDataCharacteristics(usbconf.Databits, usbconf.Stopbits, usbconf.Parity)
            dev.setBaudRate(usbconf.BaudRate)
            dev.setLatencyTimer(120)
            dev.setEventNotification(evtmask=1, evthandle=self._mywin32event.handle)
            writetimeout = usbconf.WriteTimeout
            if usbconf.WriteTimeout == -1: writetimeout = 0  #-1 is the inf timeout, convert it to 0 for ftdi compatibility
            dev.setTimeouts(int(usbconf.ReadTimeout * 1000), int(writetimeout * 1000))
            self.device = dev
            return True

        except ftd2xx.DeviceError:
            logging.warning('Problem with the device, check if Virtual COM Port is open or if wrong FTDI ID provided')
            return None

    def disconnect(self):
        try:
            self.device.close()

        except ftd2xx.DeviceError:
            logging.warning('Problem with the device, check if Virtual COM Port is open or if wrong FTDI ID provided')
            return None

    def read(self, numofbytes):
        try:
            bytesread = self.device.read(numofbytes)
            if len(bytesread) != numofbytes:
                raise UartTimeoutError
            return bytesread
        except ftd2xx.DeviceError:
            self.api_error_counter += 1
            if self.api_error_counter > 10: #10 errors max, else quiting
                sys.exit()
            raise UartApiError

    def write(self, bytestowrite):
        try:
            byteswritten = self.device.write(bytestowrite)
            if len(bytestowrite) != byteswritten:
                raise UartTimeoutError
        except ftd2xx.DeviceError:
            self.api_error_counter += 1
            if self.api_error_counter > 10: #10 errors max, else quiting
                sys.exit()
            raise UartApiError

    def flush(self):
        try:
            self.device.read(self.get_waiting)
        except ftd2xx.DeviceError:
            self.api_error_counter += 1
            if self.api_error_counter > 10: #10 errors max, else quiting
                sys.exit()
            raise UartApiError

    def get_waiting(self):
        return self.device.getQueueStatus()

    def block_and_wait_byte(self, waittime):
        """Waiting here for a new character
        We need manual reset, even if auto reset is on for win32event, the readsensors creates events.
        from doc "An event object whose state remains signaled until a single waiting thread is released,"
        Therefore thread wont stop to the first time it encounters the wait. After the first wait the event
        is reset"""
        if platform.system() == 'Linux':
            if waittime == -1 : waittime = 300 * 1000 #it is ask to wait indefinitely, add the maximum wait to 5min for failsafe
            select.select([self.device], [], [], waittime/1000.0)
            pass  # I am not sure if it works!
        elif platform.system() == 'Windows':
            import win32event
            waittime = int(waittime)
            win32event.ResetEvent(self._mywin32event)
            win32event.WaitForSingleObject(self._mywin32event, waittime)


class SerialInterface(UartInterface):
    def __init__(self, usbconf):
        self.device = None
        self.usbconf = usbconf
        self.api_error_counter = 0

    def connect(self):
        usbconf = self.usbconf
        serport = usbconf.SerialDevice
        serbaudrate = usbconf.BaudRate

        if usbconf.Parity == 0 : serparity = serial.PARITY_NONE
        elif usbconf.Parity == 1: serparity = serial.PARITY_ODD
        else: serparity = serial.PARITY_EVEN

        if usbconf.Stopbits == 1 : serstopbit = serial.STOPBITS_ONE
        else: serstopbit = serial.STOPBITS_TWO

        if usbconf.FlowControl == 0x0000: serxonxoff, serrtscts, serdsrdtr = False, False, False
        elif usbconf.FlowControl == 0x0100: serxonxoff, serrtscts, serdsrdtr = False, True, False
        elif usbconf.FlowControl == 0x0200: serxonxoff, serrtscts, serdsrdtr = False, False, True
        else: serxonxoff, serrtscts, serdsrdtr = True, False, False
        serreadtimeout = usbconf.ReadTimeout
        if usbconf.WriteTimeout == -1: serwritetimeout=None  #-1 is the inf timeout, convert it to None for pyserial compatibility
        else: serwritetimeout = usbconf.WriteTimeout
        try: #timeouts in float
            self.device = serial.Serial(port=serport,
                                         baudrate=serbaudrate,
                                         parity=serparity,
                                         stopbits=serstopbit,
                                         xonxoff=serxonxoff,
                                         rtscts=serrtscts,
                                         dsrdtr=serdsrdtr,
                                         timeout=serreadtimeout,
                                         write_timeout=serwritetimeout)
            return True
        except (ValueError, serial.SerialException):
            logging.warning('Problem with the device, check if COM Port is open or if wrong COM port provided')
            return None

    def disconnect(self):
        self.device.close()

    def read(self, numofbytes):
        try:
            bytesread = self.device.read(numofbytes)
            if len(bytesread) != numofbytes:
                raise UartTimeoutError
            return bytesread
        except serial.SerialException:
            self.api_error_counter += 1
            if self.api_error_counter > 10: #10 errors max, else quiting
                sys.exit()
            raise UartApiError

    def write(self, bytestowrite):
        try:
            byteswritten = self.device.write(bytestowrite)
            if len(bytestowrite) != byteswritten:
                raise UartTimeoutError
        except serial.SerialException:
            self.api_error_counter += 1
            if self.api_error_counter > 10: #10 errors max, else quiting
                sys.exit()
            raise UartApiError

    def flush(self):
        self.device.flushInput()

    def get_waiting(self):
        return self.device.inWaiting()

    def block_and_wait_byte(self, waittime):
        """Waiting here for a new character using select for linux and hack for windows.
        The waittime is in milliseconds! Select gets seconds.
        """
        if platform.system() == 'Linux':
            if waittime == -1 : waittime = 1800 * 1000 #it is ask to wait indefinitely, add the maximum wait to 30min for failsafe
            select.select([self.device], [], [], waittime/1000.0)
        elif platform.system() == 'Windows':
            totalwait = 0  # count the slept time
            while self.get_waiting() == 0 and (totalwait < waittime or waittime == -1): #-1 is inf waiting
                sleep(0.001) #Short artificial sleep of 50ms
                totalwait += 0.001
