__author__ = 'Georgios Lilis'
import os
import csv
import logging

class CsvInterfaceError(Exception):
    """An exception for csv handling class"""
    pass


class CsvInterface(object):
    """
    This class takes the responsibility to create the csv files in order to be writen
    It provides also the read and write funcionality to the csv files. The constructor takes
    the list of the filenames, and the list of the fieldnames that will be attached to each
    csv object that woulb be created from the filelist. fieldnameslist entries should match the
    one of the filelist. Filetags are the dictionary keys of the incomming rows to write
    so that this class can know where to write them. The advantage is that the rowlist doesnt need to
    have all the size and sorting of the contructor filelist. Also the optional delimiter can be customized.
    Finally the parameter currentdir permits the customization of desired files folder.
    If none is entered the folder of the collectors is selected
    """
    def __init__(self, mode, filelist, fieldnameslist=None, filetags=None, currentdir=None, delimiter=',', createfileifnotexist=False):
        if [x for x in filelist if filelist.count(x) > 1]:
            raise CsvInterfaceError('Error: duplicates in filelist')
        if not currentdir:
            currentdir = os.path.dirname(os.path.realpath(__file__))
        self.csvIOlist = []
        self.filehandlerslist = []  # we keep the file objects here
        self.totalfilesize = 0  #Total size of all read files in KB
        for index, filename in enumerate(filelist):
            # Check if user gave csv extension
            if filename[-4:] != '.csv':
                filename += '.csv'
            if fieldnameslist:
                fieldnames = fieldnameslist[index]
            else:
                fieldnames = None
            if mode == 'w' or mode == 'a':
                if not fieldnameslist or not fieldnames:
                    raise CsvInterfaceError('Not fieldnameslist was given, cannot parce dict to csv')
                if len(filelist) != len(fieldnameslist):
                    raise CsvInterfaceError('Filelist length is not equal with fieldnamelist length')
                if not filetags or len(filelist) != len(filetags):
                    raise CsvInterfaceError('Filetags problem, cannot construct the object')

                self.filetags = filetags
                # Check if mode is a but no file exists, if not change mode to write
                try:
                    if not os.path.exists(currentdir + '/' + filename) and mode == 'a':
                        mode = 'w'
                        # Create file(open with w) since it doesnt exist
                        csvfile = open(currentdir + '/' + filename, mode + 'b', buffering=0)  # buffering per line
                        self.totalfilesize += os.path.getsize(currentdir + '/' + filename)/1024
                        self.filehandlerslist.append(csvfile)
                        csvio = csv.DictWriter(csvfile, fieldnames=fieldnames, delimiter=delimiter,
                                               quotechar='|', quoting=csv.QUOTE_MINIMAL, extrasaction='raise')
                        csvio.writeheader()
                        mode = 'a'  # Restore the mode back again in case other files(in the next loop) exist
                    else:
                        csvfile = open(currentdir + '/' + filename, mode + 'b', buffering=0)  # buffering per line
                        self.totalfilesize += os.path.getsize(currentdir + '/' + filename)/1024
                        self.filehandlerslist.append(csvfile)
                        csvio = csv.DictWriter(csvfile, fieldnames=fieldnames, delimiter=delimiter,
                                               quotechar='|', quoting=csv.QUOTE_MINIMAL, extrasaction='raise')
                        if mode == 'w':  # write the header only in empty file with 'w' not in append mode
                            csvio.writeheader()
                except IOError:
                    logging.exception('Error opening the csv file for write' + currentdir + '/' + filename)
                    raise IOError(currentdir + '/' + filename)

            elif mode == 'r':
                if not os.path.exists(currentdir + '/' + filename) and createfileifnotexist:
                    #we need fieldnames to create the files
                    if not fieldnameslist or not fieldnames:
                        raise CsvInterfaceError('Not fieldnameslist was given, cannot parce dict to csv')
                    if len(filelist) != len(fieldnameslist):
                        raise CsvInterfaceError('Filelist length is not equal with fieldnamelist length')
                    if not filetags or len(filelist) != len(filetags):
                        raise CsvInterfaceError('Filetags problem, cannot construct the object')
                    # Create file(open with w) since it doesnt exist
                    csvfile = open(currentdir + '/' + filename, 'wb', buffering=0)  # buffering per line
                    csvio = csv.DictWriter(csvfile, fieldnames=fieldnames, delimiter=delimiter,
                                           quotechar='|', quoting=csv.QUOTE_MINIMAL, extrasaction='raise')
                    csvio.writeheader()
                    csvfile.close()
                try:
                    csvfile = open(currentdir + '/' + filename, 'rb')
                    self.totalfilesize += os.path.getsize(currentdir + '/' + filename)/1024
                    self.filehandlerslist.append(csvfile)
                except IOError:
                    logging.exception('Error opening the csv file for read' + currentdir + '/' + filename)
                    raise IOError('Error opening the csv file for read  ' + currentdir + '/' + filename)
                csvio = csv.DictReader(csvfile, delimiter=delimiter, quotechar='|', quoting=csv.QUOTE_MINIMAL)
            else:
                raise CsvInterfaceError('Not supported mode {0}'.format(mode))

            self.csvIOlist.append(csvio)

    def write_to_csv(self, rowlist):
        """
        This function accepts a list of dictionaries of dictionary each line correspond to a name
        of the file(given by outer dict). They dont have to be in sync due to the outer dict. It then writes each
        line to each required file. The rowlist look like [{'':{...}}]. The outer dict is for filetag to write
        and the internal for the specific collumns. Outer dict has only one key, the filetag
        of the file to write(.keys()[0]). The row of collumns is row.items[1]. The strong point of the writer is the
        possibility to feed him with mixed samples for different files, and he will put it in the correct, depending
        on the dictionary key.
        """
        if not rowlist or len(rowlist) == 0:
            logging.warning('Caution no rowlist provided or list is empty')
            return
        for row in rowlist:
            if row:  # external dict is here in row
                if row.keys()[0] not in self.filetags:
                    logging.warning('Caution a row that was asked to be written not in filetags: '+str(row.keys()[0]))
                    continue
                indexinfilelist = self.filetags.index(row.keys()[0])  # find the correct fileindex
                csvIO = self.csvIOlist[indexinfilelist]  # thus choose the correct csvIO object
                try:
                    # The row returns the list (keys, values), we have only one key, so [0]. [1] to take the values
                    rowvalues = row.items()[0][1]
                    assert set(csvIO.fieldnames) == set(rowvalues.keys())
                    csvIO.writerow(rowvalues)
                except AssertionError:
                    logging.error('Error, keys of dictionary of passed row dont match the fieldname of the constructor')
                    raise CsvInterfaceError('Error, keys of dictionary of passed row dont match the fieldname of the constructor')

    def read_from_csv(self, truncate=0):
        """
        This function returns a list of lists of rows. This means the external list corresponds to
        the class's filenames and the internal list is the list of rows. Each row is a dict with keys the fieldnamelist
        of the specific file. The reader returns list in the order of asked filelist, it doesnt need a filetag.
        """
        filesrowslist = []
        for csvio in self.csvIOlist:
            rowslist = list(csvio)
            row = dict()
            if len(rowslist) == 0:  # Nothing in file.
                for x in csvio.fieldnames:
                    row[x] = float("NaN")
                rowslist.append(row)
            elif truncate > 0:
                rowslist = rowslist[-1*truncate:]
            filesrowslist.append(rowslist)
        return filesrowslist

    def read_last_from_csv(self):
        """
        This function returns a list of lists of rows. This means the external list corresponds to
        the class's filenames and the internal list is the list of rows. Each row is a dict with keys the fieldnamelist
        of the specific file. The reader returns list in the order of asked filelist, it doesnt need a filetag.
        """
        fileslastrowlist = []
        for csvio in self.csvIOlist:
            csvrows = list(csvio)
            lastrow = dict()
            if len(csvrows) > 0:
                lastrow = csvrows[-1]
            else:
                for x in csvio.fieldnames:
                    lastrow[x] = float("NaN")
            fileslastrowlist.append(lastrow)
        return fileslastrowlist

    def close_all_csv(self):
        """
        close all files, CsvIOlist is now invalid.
        We call the self destructor also to free the memory.
        """
        for filehandler in self.filehandlerslist:
            filehandler.close()
        self.csvIOlist = []
        del self