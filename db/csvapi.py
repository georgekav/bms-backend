__author__ = 'Georgios Lilis'
"""Various csv utilities"""
import copy
from math import isnan
from csvinterface import CsvInterface, CsvInterfaceError
from conf_db import *
import atexit
import time
import logging

class CsvApiError(Exception):
    """An exception to handle csv utilities exemptions that would pass to higher classes"""
    pass

class CsvApi(object):
    def __init__(self, filelist, flushinterval = CSV_FLUSH_INTERVAL, nobuffer=False, truncate=LAST_ROWS_NUMBER):
        """
        CsvApi provides all the tools for reading and writing to the csv files <b>specific</b> to  our application.
        It is not a general csv parser, this is the job of the CsvInterface. Here all the functionality of a <u>csv</u>
        specific "db" store for a bms will be implemented. This Api infact emulates the timeseries DB functionality,
        like taking time blocks, buffering etc.

        flushinterval: An option of how prequently the CsvApi will flush its buffer to the csv files.
        truncate: keep only the last x number of entries in the list
        """
        self.CSV_FIELDS = ('Time', 'Value')
        self._nobuffer = nobuffer
        self._truncatenum = truncate
        readwritelist = []
        filetaglist = []
        fieldlist = []
        #You have to manually create the files if you add a new sensor
        for filename in filelist:
            readwritelist.append(filename)
            filetaglist.append(filename[:-4])
            fieldlist.append(self.CSV_FIELDS)

        if not nobuffer:
            #Initialize the interface and reader, and read all the files.
            #Add them to the memory buffer for fast access.
            try:
                csvreader = CsvInterface(mode='r', filelist=readwritelist,filetags=filetaglist,
                                         fieldnameslist=fieldlist, currentdir=DB_STORE_DIR, createfileifnotexist=True)
            except CsvInterfaceError:
                logging.error('Failed to create a csvreader with: {0}'.format(readwritelist))
                raise CsvApiError
            logging.info('Loading {0} csv files entries'.format(len(readwritelist)))
            starttime = time.time()
            allrows = csvreader.read_from_csv(truncate=self._truncatenum) # Buffer only the last xxxx values
            logging.info("Total of {0} KB in CSV format loaded in {1} sec".format(csvreader.totalfilesize, round(time.time()-starttime,3)))
            csvreader.close_all_csv()  # Destroys the csvreader
            self.csvmembuffer = dict()
            for filerows, filetag in zip(allrows, filetaglist):
                self.csvmembuffer[filetag] = filerows
        try:
            self.csvwriter = CsvInterface(mode='a', filelist=readwritelist,filetags=filetaglist,
                                          fieldnameslist=fieldlist,currentdir=DB_STORE_DIR)
        except CsvInterfaceError:
            logging.error('Failed to create a csvwriter with: {0}'.format(readwritelist))
            raise CsvApiError
        self.rowspendingwrite = []
        self.flushinterval = flushinterval
        atexit.register(func=self.csv_data_writer, rowstoappend = [], forced = True)

    def csv_block_data_reader(self, filetag, initendtime, resolution):
        """
        This method get a filetag and returns a list. The  list is the requested
        file rows from inittime to endtime. Each row is a dictionary. Time, Value for sensors ;
        Time, S, PF for loads. inittime, endtime in unix time format. The output doesnt follow the csvwriter format!!
        If endtime=-1 then gives all they exist from init to EOF. If init=-1 then returns from the SOF(start of file) to endtime.
        If both are -1 parce all existing data
        If inittime==endtime then you get a datapoint with exactly the time requested
        If resolution==0 then you get all the data points in the files without aggregation
        """
        (inittime, endtime) = initendtime

        if endtime == -1:
            endtime = float('inf')  # make it inf if we have -1 as end time
        if inittime > endtime:
            raise CsvApiError('Initial time is greater than end')

        filerowsparsed = []
        fieldnames = self.CSV_FIELDS
        try:
            if self._nobuffer:
                logging.info('Register request to read file: {0} For time: [{1} - {2}] Res: {3}'.format(filetag, inittime, endtime, resolution))
                try:
                    csvreader = CsvInterface(mode='r', filelist=[filetag+'.csv'], currentdir=DB_STORE_DIR)
                except CsvInterfaceError:
                    logging.error('Failed to create a csvreader with: {0}'.format([filetag+'.csv']))
                    raise CsvApiError
                filerows = csvreader.read_from_csv()[0]  # We have one file request "[0]", filerows are all str
                csvreader.close_all_csv()
                #Appending the new not yet written values
                for row in self.rowspendingwrite:
                    if filetag in row:
                        filerows.append(row[filetag])
            else:
                try:
                    filerows = self.csvmembuffer[filetag]
                except KeyError as key:
                    raise CsvApiError('Requested a read for sensor {0} whose file doesnt exist'.format(key))
                #We need more in the past, reread the file from day one
                if float(filerows[0]['Time']) > inittime:
                    logging.info('Data not in memory, Register request to read file: {0} For time: [{1} - {2}] Res: {3}'.format(filetag, inittime, endtime, resolution))
                    try:
                        csvreader = CsvInterface(mode='r', filelist=[filetag+'.csv'], currentdir=DB_STORE_DIR)
                    except CsvInterfaceError:
                        logging.error('Failed to create a csvreader with: {0}'.format([filetag+'.csv']))
                        raise CsvApiError
                    filerows = csvreader.read_from_csv()[0]  # We have one file request "[0]", filerows are all str
                    csvreader.close_all_csv()
                    #Appending the new not yet written values
                    for row in self.rowspendingwrite:
                        if filetag in row:
                            filerows.append(row[filetag])

            average = {}
            for field in fieldnames:
                average[field] = 0  # initialize
            count = 0
            previoustime = float(filerows[0]['Time'])
            endoffiletime = float(filerows[-1]['Time'])
            if resolution < 0:
                raise CsvApiError('Resolution < 0')
            elif resolution == 0 and inittime != endtime:
                resolution = 0.001  # manualy override the resolution to 1ms, if inittime==endtime then Dont Care
            # When the requested initial time is beyond the end of file, return the last entry
            if inittime > endoffiletime:
                logging.warning('Nothing found starting from(inittime): '+str(inittime) + ' For file: ' +
                             filetag+'.csv' + ' Last time sample: ' + filerows[-1]['Time']+' returning that.')
                # Give a 1 entry list with the lastest value found
                for field in fieldnames:
                    average[field] = float(filerows[-1][field])  # Give a 1 entry list with the lastest value found
                filerowsparsed.append(average)
                return filerowsparsed
            for i in range(len(filerows)):  # Accessing all file's rows
                if inittime <= float(filerows[i]['Time']) <= endtime:  # Start from now taking averages
                    if count == 0:
                        # initialize only once, it gets here in the first true assertion of inittime <= float(filerows[i]['Time'])
                        # if the time is not exactly the start time, then we start from 1 sample before,
                        # async aggregation utilized in collection means that the current value at this time is the previous
                        # A check is introduced in case that no previous sample can be found due to the start of file
                        if i != 0 and inittime != float(filerows[i]['Time']):
                            previoustime = float(filerows[i - 1]['Time'])
                            for field in fieldnames:
                                average[field] = float(filerows[i - 1][field])
                            starttimenum = i - 1
                            count += 1  # we cound it
                        else:
                            previoustime = float(filerows[i]['Time'])
                            starttimenum = i
                    if float(filerows[i]['Time']) <= previoustime + resolution:
                        count += 1
                        for field in fieldnames:
                            if field != 'Time':  # Ignore time we should do with middle point here
                                average[field] += float(filerows[i][field])
                    else:
                        endtimenum = i - 1  # The end is the previous i since we dont count this
                        for field in fieldnames:
                            if field != 'Time':
                                average[field] = round(average[field] / count, 3)
                            elif field == 'Time':
                                # For time I take middle point
                                average[field] = round(float(filerows[starttimenum]['Time']) +
                                                       (float(filerows[endtimenum]['Time']) -
                                                        float(filerows[starttimenum]['Time'])) / 2.0, 3)
                        filerowsparsed.append(copy.deepcopy(average))
                        count = 1  # we count the current sample
                        starttimenum = i  # And we set the start again
                        for field in fieldnames:
                            if field != 'Time':
                                average[field] = float(filerows[i][field])  # reinitialize the average,we count the current sample
                            elif field == 'Time':  # Not really needed since we overwrite
                                average[field] = 0
                        previoustime = float(filerows[i]['Time'])
                elif float(filerows[i]['Time']) > endtime or isnan(float(filerows[i]['Time'])):
                    # Stop reading the file, also here will enter the loop when inittim==endtime
                    # write current average buffer and break
                    if count == 0:
                        if inittime == endtime:  # datapoint request. give the direct ancestor.
                            for field in fieldnames:
                                average[field] = float(filerows[i - 1][field])
                        else:
                            logging.warning("Nothing found for this range of time for file: " + filetag)
                            # Give a 1 entry list with the direct ancestor.
                            for field in fieldnames:
                                average[field] = float(filerows[i -1][field])
                        filerowsparsed.append(average)
                        break  # break row parsing and continue to next file if requested something from future
                    endtimenum = i - 1  # The end is the previous i since we dont count this
                    for field in fieldnames:
                        if field != 'Time':
                            average[field] = round(average[field] / count, 3)
                        elif field == 'Time':
                            # For time I take middle point
                            average[field] = round(float(filerows[starttimenum]['Time']) +
                                                   (float(filerows[endtimenum]['Time']) -
                                                    float(filerows[starttimenum]['Time'])) / 2.0, 3)
                    filerowsparsed.append(copy.deepcopy(average))
                    break
                if float(filerows[i]['Time']) <= endtime and i == len(filerows) - 1:
                    # The endtime is bigger than the recorded
                    # record the last line before return
                    if count != 1:
                        endtimenum = i - 1  # The end is the previous i since we dont count this
                    else:
                        endtimenum = i  # It is the last and we dont have dt(high resolution) so we save the last value time
                    for field in fieldnames:
                        if field != 'Time':
                            average[field] = round(average[field] / count, 3)
                        elif field == 'Time':
                            # For time I take middle point
                            average[field] = round(float(filerows[starttimenum]['Time']) +
                                                   (float(filerows[endtimenum]['Time']) -
                                                    float(filerows[starttimenum]['Time'])) / 2.0, 3)
                    filerowsparsed.append(copy.deepcopy(average))
            return filerowsparsed
        except KeyError as e:
            logging.exception("Key" + repr(e) + "not found. Block reading failed")
            return False

    def csv_last_data_reader(self, filetag):
        """Gets the last entry from the file in the filetag"""
        try:
            if self._nobuffer:
                logging.info('Register req to read file: {0}'.format(filetag))
                try:
                    csvreader = CsvInterface(mode='r', filelist=[filetag+'.csv'], currentdir=DB_STORE_DIR)
                except CsvInterfaceError:
                    logging.error('Failed to create a csvreader with: {0}'.format([filetag+'.csv']))
                    raise CsvApiError
                rows = csvreader.read_from_csv()[0]  # We have one file request "[0]"
                for row in self.rowspendingwrite:
                    if filetag in row:
                        rows.append(row[filetag])
                lastrowstr = rows[-1]
                csvreader.close_all_csv()
            else:
                try:
                    lastrowstr = self.csvmembuffer[filetag][-1]
                except KeyError as key:
                    raise CsvApiError('Requested a read for sensor {0} whose file doesnt exist'.format(key))

            lastrow = dict()
            for field in self.CSV_FIELDS:  #We convert to float
               lastrow[field] = float(lastrowstr[field])
            return [lastrow]  # Always return a list of 1 to be compatible
        except KeyError as e:
            logging.exception("Key" + repr(e) + "not found. Last reading failed")
            return False

    def csv_data_writer(self, rowstoappend, forced=False):
        """
        This function add to the memory buffer the new incomming values. After that, it checks if the flushinterval
        passed and performs a disk write to csv files.
        """
        if not self._nobuffer:
            try:
                for row in rowstoappend:
                    filetag = row.keys()[0]
                    self.csvmembuffer[filetag].append(row.items()[0][1])
                    #Auto truncate as more data accumulate
                    if len(self.csvmembuffer[filetag]) > self._truncatenum + 500:
                        self.csvmembuffer[filetag] = self.csvmembuffer[filetag][-1*self._truncatenum:]
                        logging.info('Removing from memory excess entries of {0}'.format(filetag))
            except KeyError as key:
                raise CsvApiError('Requested a write for sensor {0} whose file doesnt exist'.format(key))
        self.rowspendingwrite.extend(rowstoappend)
        if len(self.rowspendingwrite) >= self.flushinterval or (forced and len(self.rowspendingwrite) > 0):
            try:
                self.csvwriter.write_to_csv(self.rowspendingwrite)
            except CsvInterfaceError:
                logging.error('Failed to write to csv')
                raise CsvApiError
            logging.info('{0} rows exported'.format(len(self.rowspendingwrite)))
            self.rowspendingwrite = []