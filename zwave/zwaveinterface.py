import time
import os
import logging
from conf_zwave import ZWAVE_HEADER_ACK, ZWAVE_HEADER_NACK, ZWAVE_HEADER_SOF, ZWAVE_HEADER_CAN, COMM_MAX_RETRIES, IS_USBZWAVE_MODULE
from uart import UartTimeoutError, UartApiError
from bitstring import BitStream, ConstBitStream,  Bits, ReadError, InterpretError
from lxml import etree
import random

Zwavedir = os.path.dirname(os.path.realpath(__file__))

#The following are the defined primary controller SerialAPI commands
ZWAVE_CMD_SET_LEARN_MODE = '50'
ZWAVE_CMD_DISCOVERYNODES = '02'
ZWAVE_CMD_APPLICATIONCOMMANDHANDLER = '04'
ZWAVE_CMD_SENDDATA = '13'
ZWAVE_CMD_GETVERSION = '15'
ZWAVE_CMD_MEMORYGETID = '20'
ZWAVE_CMD_GETNODEPROTOCOLINFO = '41'
ZWAVE_CMD_SETDEFAULT = '42'
ZWAVE_CMD_APPLICATION_UPDATE = '49'
ZWAVE_CMD_ADD_NODE_TO_NETWORK = '4a'
ZWAVE_CMD_REMOVE_NODE_FROM_NETWORK = '4b'

ZWAVE_CMD_SOFT_RESET = '08'
ZWAVE_CMD_HARD_RESET = '42'

def repetition_decorator(f):
    """A decorator to auto repeat transmition in case of failure.
    The functions it decorates either timeout, give a ZwaveInterfaceError or return False/None"""
    def wrapper(*args, **kwargs):
        if not args[0].status:
            logging.error("UART device not open, unable to proceed")
            return
        watchdog = 0
        while watchdog < COMM_MAX_RETRIES:
            try:
                ret = f(*args, **kwargs)
                if ret:
                    return ret
                else:
                    watchdog += 1
                    continue
            except (UartTimeoutError, ZwaveInterfaceError, UartApiError):
                time.sleep(random.randint(1,10))  #Sleep Random 1 to 10.0 sec
                watchdog += 1
                continue
            except RepetitionTermination:
                #At this exception terminate all the following repetitions.
                return False
        logging.error('Communication failed after {0} tries'.format(COMM_MAX_RETRIES))
        return False
    return wrapper

class RepetitionTermination(Exception):
    """Exception to handle the interface termination for repetition"""
    pass

class ZwaveInterfaceError(Exception):
    """Exception to handle the interface errors"""
    pass

class ZwaveInterface(object):
    def __init__(self, uartapi):  # esmartaddr is the esmart plug that has the master zwave module
        self.uartapi = uartapi
        if self.uartapi.device:
            self.status = True
        else:
            self.status = False
        self._previousframe = None
        try:
            xmlfile = os.path.join(Zwavedir, "zwave_custom_cmd_classes.xml")
            tree = etree.parse(xmlfile)
            self._commandclasses = tree.getroot()
            xmlfile = os.path.join(Zwavedir, "zwave_sensorstypes.xml")
            tree = etree.parse(xmlfile)
            sensorstypes = tree.getroot()
            self._multilevelsensors = sensorstypes.find("multilevel_sensor_types")
        except IOError as e:
            logging.exception(repr(e))

    def _send_ack(self, num=1):
        """ Send num times of ACK, the num is necessary when
        trying to clean channel and stop everybody form broadcasting"""
        if not self.status:
            logging.error("UART device not open, unable to read")
            return
        for i in range(num):
            self.uartapi.write(ZWAVE_HEADER_ACK)

    def _send_nack(self):
        """Inform the sensor to resend the packet"""
        if not self.status:
            logging.error("UART device not open, unable to read")
            return
        self.uartapi.write(ZWAVE_HEADER_NACK)

    def _get_ack(self):
        """Return true if we got an ACK, False otherwise"""
        ackmsg = self.uartapi.read(1)  # We read the ACK
        if ackmsg == ZWAVE_HEADER_ACK:
            return True
        elif ackmsg == ZWAVE_HEADER_CAN:
            logging.warning('CAN received, sending ACK to the controller that expects it')
            self._send_ack()
            return False
        elif ackmsg == ZWAVE_HEADER_NACK:
            logging.warning('NACK received')
            return False
        elif ackmsg:
            logging.warning('Input buffer packet collision')
            self.uartapi.flush()
            return False
        else:
            logging.warning('Nothing received')
            return False

    def _send_request(self, string):
        """Creates the final packet for sending"""
        buffervl = self._payload_to_frame(string)  # append sof lengh and REQ(type) + CRC
        self.uartapi.flush() #Empty the input buffer in order to wait for the new packet
        self.uartapi.write(buffervl.bytes)
        logging.debug("SEND %02d:%02d:%02d %s "
                      % (time.localtime()[3], time.localtime()[4], time.localtime()[5], buffervl.hex))
        if not self._get_ack():
            logging.warning('Request sending failed')
            raise ZwaveInterfaceError

    def _receive_response(self):
        """It receives response to a command initialized to zw network"""
        firstbyte = self.uartapi.read(1)
        if firstbyte == ZWAVE_HEADER_SOF:
            # We got the SOF and we wait for the rest of message
            # (SOF) + LEN + ... (sof is already read)
            length = BitStream(bytes=self.uartapi.read(1))
            crclength = 1
            payloadlen = (length.uint - 1) + crclength  # Length counts also lenght byte
            rawmsg = ZWAVE_HEADER_SOF + length.bytes + self.uartapi.read(payloadlen)
            # In CRC the SOF ( and CRC of course) is out,
            # SOF is handled internally to the function, CRC here
            rawcrc = self._calc_crc(BitStream(bytes=rawmsg[0:-1]))
            logging.debug("RECEIVE %02d:%02d:%02d %s "
                          % (time.localtime()[3],time.localtime()[4],time.localtime()[5],BitStream(bytes=rawmsg).hex))
            if rawcrc == BitStream(bytes=rawmsg[-1]).uint:
                self._send_ack()
                return rawmsg
            else:
                self._send_nack()
                logging.debug("CRC check failed ignoring packet: " + BitStream(bytes=rawmsg).hex)
                raise ZwaveInterfaceError
        else:
            logging.error('Response reception failed')
            raise ZwaveInterfaceError

    def _confirm_zwcmd_send(self, funcid):
        """This function handles the handshare between the master modules
         and sensor to get or set sync information to the endpoint"""

        decodedresponse = self._unpack_dataframe(self._receive_response())
        serialapi_cmd = ZWAVE_CMD_SENDDATA
        self._verify_serialapi_cmd(decodedresponse, serialapi_cmd)

        if not decodedresponse['PARAMETERS'] == '01':
            raise ZwaveInterfaceError

        decodedresponse = self._unpack_dataframe(self._receive_response())

        self._verify_serialapi_cmd(decodedresponse, serialapi_cmd)
        self._verify_funcid(decodedresponse, funcid)

        if not decodedresponse['PARAMETERS'][2:4] == '00':
            logging.info('GET/SET cmd FAILED')
            raise ZwaveInterfaceError

    @repetition_decorator
    def send_primary_hard_reset(self):
        """It resets the primary controller and destroys the network. Set internal EPPROM to defaults."""
        serialapi_cmd = ZWAVE_CMD_HARD_RESET
        funcid = self.gen_funcid()
        self._send_request(serialapi_cmd + funcid)
        logging.info('Primary controller reset to default')
        logging.info('Waiting 10sec for IC to initialize')
        time.sleep(10)
        return True

    @repetition_decorator
    def send_primary_soft_reset(self):
        """It soft resets the primary controller."""
        if not IS_USBZWAVE_MODULE:
            self._send_request(ZWAVE_CMD_SOFT_RESET)
            logging.info('Primary controller soft reset')
            logging.info('Waiting 5sec for IC to initialize')
            time.sleep(5)
        return True

    @repetition_decorator
    def send_primary_wake(self):
        """Important for keeping the primary controller on and not sleeping. In fact we just ask the basic command to get
        version"""
        serialapi_cmd = ZWAVE_CMD_GETVERSION
        self._send_request(serialapi_cmd)
        # the answer from static controller. Fixed for ZM500, it gives the version infos, no need to explicitly parse it
        response = self._receive_response()
        nameread = response[4:10]
        versionread = response[4:15]
        if nameread == 'Z-Wave':
            logging.info('Zwave primary controller with version {0} replied to initial packet'.format(versionread))
            return True  # The only possible positive escape
        else:
            return False

    @repetition_decorator
    def set_learn_mode(self):
        """Equivalent of button press, used by the ZM controller"""
        serialapi_cmd = ZWAVE_CMD_SET_LEARN_MODE
        mode = '01' #ZW_SET_LEARN_MODE_NWI
        funcid = self.gen_funcid()
        self._send_request(serialapi_cmd + funcid + mode)
        logging.debug("Learn mode started")


    @repetition_decorator
    def get_node_list(self):
        """This function returns the node list, [int] that is inside the EEPROM of primary controller.
        The controller returns a nodelist as bit mask of length 29. """
        serialapi_cmd = ZWAVE_CMD_DISCOVERYNODES
        self._send_request(serialapi_cmd)
        decodedresponse = self._unpack_dataframe(self._receive_response())
        self._verify_serialapi_cmd(decodedresponse, serialapi_cmd)
        nodelist = []
        nodesraw = BitStream(hex=decodedresponse['PARAMETERS'][6:-6])
        bytecounter = 0
        while nodesraw.pos < nodesraw.len:
            nodebyte = BitStream(hex=nodesraw.read('hex:8'))
            nodebyte.reverse()
            while nodebyte.pos < nodebyte.len:
                nodebit = nodebyte.read('bool')
                if nodebit:
                    nodeid = nodebyte.pos + bytecounter*8
                    nodelist.append(nodeid)
            bytecounter += 1
        return nodelist

    @repetition_decorator
    def get_node_protocol_info(self, nodeid):
        """This function returns the node protocol information provided by the node itself.
        """
        nodeid = self._hex_normalizer(nodeid)
        serialapi_cmd = ZWAVE_CMD_GETNODEPROTOCOLINFO
        self._send_request(serialapi_cmd+nodeid)
        decodedresponse = self._unpack_dataframe(self._receive_response())
        self._verify_serialapi_cmd(decodedresponse, serialapi_cmd)
        nodeprotocol = dict()
        nodeprotocol['Capabilities'] = decodedresponse['PARAMETERS'][:2]
        if ConstBitStream(hex=decodedresponse['PARAMETERS'][:2]).read('bool'): nodeprotocol['Always Listening'] = True
        else: nodeprotocol['Always Listening'] = False
        nodeprotocol['Security'] = decodedresponse['PARAMETERS'][2:4]
        nodeprotocol['Basic'] = self._commandclasses.findall("bas_dev[@key='0x" + decodedresponse['PARAMETERS'][6:8] + "']")[-1].attrib.get('name')
        genericclass = decodedresponse['PARAMETERS'][8:10]
        nodeprotocol['Generic'] = self._commandclasses.findall("gen_dev[@key='0x" + genericclass + "']")[-1].attrib.get('name')
        specificclass = decodedresponse['PARAMETERS'][10:12]
        nodeprotocol['Specific'] = self._commandclasses.findall("gen_dev[@key='0x" + genericclass + "']")[-1].find("spec_dev[@key='0x" + specificclass + "']").attrib.get('name')
        return nodeprotocol

    @repetition_decorator
    def get_network_id(self):
        """This command will return the HomeID of zwave network in which the primary controller belongs"""
        serialapi_cmd = ZWAVE_CMD_MEMORYGETID
        self._send_request(serialapi_cmd)
        decodedresponse = self._unpack_dataframe(self._receive_response())
        self._verify_serialapi_cmd(decodedresponse, serialapi_cmd)
        homeid = decodedresponse['PARAMETERS'][:-2] #$ bytes
        return homeid

    @repetition_decorator
    def get_network_message(self):
        """The main method to read the zwave packets, it issues an ACK when a correct packet is received.
        This covers all the range of the command classes possible in the PAYLOAD. It has some cmd class interpretation
        implemented as well. This method will not treat the HOST >>or<< Primary controller network commands, only
        the payloads with zwave command class commint from remote endpoint. This is usually for receiving meaningful
        remote application layer data.
        In this function RepetitionTermination is called at error since nothing to repeat, it is just a collecting func"""
        finalrawmsg = self._receive_response()
        currenttime = time.time()
        decodedresponse = self._unpack_dataframe(finalrawmsg)  #One frame returned
        if decodedresponse != self._previousframe:
            self._previousframe = decodedresponse
        else:
            logging.debug("Frame repetition")
            raise RepetitionTermination
        # Read the frames
        if decodedresponse['COMMAND'] == ZWAVE_CMD_APPLICATIONCOMMANDHANDLER:
            ret = self._unpack_application_command(decodedresponse['PARAMETERS'])
            if ret: #ret can be None if _unpack_application_command find a dublicate broacast frame
                nodeid, cmdclasspayload = ret
                cmdclass, command, data = self._unpack_command_class(cmdclasspayload)
            else:
                logging.debug("Cannot unpack application command class: {0}".format(decodedresponse['PARAMETERS']))
                raise RepetitionTermination
            info = self._interpret_command_class(cmdclass, command, data)
            if info:  # Check for same data
                logging.debug(info)
                decoded_data = (currenttime, nodeid, info)
                return decoded_data
        else:
            logging.debug("No Application Command Handler byte was found in frame, no remote data to process")
            raise RepetitionTermination

    @repetition_decorator
    def add_node(self):
        """Adds the node that will get in learn mode, aka button press"""
        self.send_primary_soft_reset()
        oldnodelist = self.get_node_list()
        serialapi_cmd = ZWAVE_CMD_ADD_NODE_TO_NETWORK
        bmode = '81' #0x80 OPTION_HIGH_POWER || 0x01 ADD_NODE_ANY
        funcid = self.gen_funcid()
        self._send_request(serialapi_cmd + bmode + funcid)
        newnode = None
        while True:
            decodedresponse = self._unpack_dataframe(self._receive_response())
            self._verify_serialapi_cmd(decodedresponse, serialapi_cmd)
            self._verify_funcid(decodedresponse, funcid)
            statusret = decodedresponse['PARAMETERS'][2:4]
            if statusret == '01':
                logging.info('Received ADD_NODE_STATUS_LEARN_READY')
                self.uartapi.block_and_wait_byte(waittime=-1)
                continue
            elif statusret == '02':
                logging.info('Received ADD_NODE_STATUS_NODE_FOUND')
                self.uartapi.block_and_wait_byte(waittime=-1)
                continue
            elif statusret == '03':
                logging.info('Received ADD_NODE_STATUS_ADDING_SLAVE')
                self.uartapi.block_and_wait_byte(waittime=-1)
                continue
            elif statusret == '05': #0x80 OPTION_HIGH_POWER || 0x05 ADD_NODE_STOP
                logging.info('Received ADD_NODE_STATUS_PROTOCOL_DONE')
                newnode = decodedresponse['PARAMETERS'][4:6]
                bmode = '85'
                funcid = self.gen_funcid()
                self._send_request(serialapi_cmd + bmode + funcid)
                self.uartapi.block_and_wait_byte(waittime=-1)
                continue
            elif statusret == '06':
                logging.info('Received ADD_NODE_STATUS_DONE')
                newnode = decodedresponse['PARAMETERS'][4:6]
                break
            elif statusret == '07':
                logging.info('Received ADD_NODE_STATUS_FAILED')
                raise ZwaveInterfaceError
        if newnode:
            if int(newnode,16) not in oldnodelist:
                nodeinfo = self.get_node_protocol_info(newnode)
                logging.info('Registered new node {0} with following properties {1}'.format(newnode, nodeinfo))
                return True
            else:
                logging.info('Node {0} already in network'.format(newnode))
                return True

    @repetition_decorator
    def remove_node(self):
        """Removes the node that will get in learn mode, aka button press"""
        self.send_primary_soft_reset()
        serialapi_cmd = ZWAVE_CMD_REMOVE_NODE_FROM_NETWORK
        bmode = '01' #0x80 OPTION_HIGH_POWER || 0x01 REMOVE_NODE_ANY
        funcid = self.gen_funcid()
        self._send_request(serialapi_cmd  +bmode + funcid)
        oldnode = None
        while True:
            decodedresponse = self._unpack_dataframe(self._receive_response())
            self._verify_serialapi_cmd(decodedresponse, serialapi_cmd)
            self._verify_funcid(decodedresponse, funcid)
            statusret = decodedresponse['PARAMETERS'][2:4]
            if statusret == '01':
                logging.info('Received REMOVE_NODE_STATUS_LEARN_READY')
                self.uartapi.block_and_wait_byte(waittime=-1)
                continue
            elif statusret == '02':
                logging.info('Received REMOVE_NODE_STATUS_NODE_FOUND')
                self.uartapi.block_and_wait_byte(waittime=-1)
                continue
            elif statusret == '03':
                logging.info('Received REMOVE_NODE_STATUS_ADDING_SLAVE')
                self.uartapi.block_and_wait_byte(waittime=-1)
                continue
            elif statusret == '04':
                logging.info('Received REMOVE_NODE_STATUS_ADDING_CONTROLLER')
                self.uartapi.block_and_wait_byte(waittime=-1)
                continue
            elif statusret == '05': #0x80 OPTION_HIGH_POWER || 0x05 REMOVE_NODE_STOP
                logging.info('Received REMOVE_NODE_STATUS_PROTOCOL_DONE')
                oldnode = decodedresponse['PARAMETERS'][4:6]
                bmode = '85'
                funcid = self.gen_funcid()
                self._send_request(serialapi_cmd + bmode + funcid)
                self.uartapi.block_and_wait_byte(waittime=-1)
                continue
            elif statusret == '06':
                logging.info('Received REMOVE_NODE_STATUS_DONE')
                oldnode = decodedresponse['PARAMETERS'][4:6]
                break
            elif statusret == '07':
                logging.info('Received REMOVE_NODE_STATUS_FAILED')
                raise ZwaveInterfaceError

        newnodelist = self.get_node_list()
        if oldnode:
            if int(oldnode,16) != 0 and int(oldnode,16) not in newnodelist:
                logging.info('Removed node {0}'.format(oldnode))
                return True
            else:
                logging.info('Node NOT in the network')
                return True

    @repetition_decorator
    def wait_button_press(self, nodeid):
        """This function stays in loop until the wakeup packet arrives from the zwave sensor. This packet is triggered
        usually by button press"""
        nodeid = self._hex_normalizer(nodeid)
        logging.info('Press the endpoint {0} button to continue'.format(nodeid))
        while True:
            try:
                decodedresponse = self._unpack_dataframe(self._receive_response())
                serialapi_cmd = ZWAVE_CMD_APPLICATION_UPDATE
                self._verify_serialapi_cmd(decodedresponse, serialapi_cmd)
                cmd_class = '84'
                if not decodedresponse['PARAMETERS'][:2] == cmd_class:
                    raise ZwaveInterfaceError
                if decodedresponse['PARAMETERS'][2:4] == nodeid:
                    logging.info("Received the wake frame from sensor, proceding....")
                    self._send_ack()
                    return True
                else:
                    logging.info("Got wake frame from node {0}".format(decodedresponse['PARAMETERS'][2:4]))
                    raise ZwaveInterfaceError
            except (UartTimeoutError, ZwaveInterfaceError):
                self._send_ack()
                self.uartapi.flush()
                continue  #we want an endles loop

    @repetition_decorator
    def wait_wakeup_notification(self, nodeid):
        """Block and wait for the WAKE_UP_NOTIFICATION that means the endpoint is active and listen for a short time.
        This message is essential before we send async data to the endpoints"""
        nodeid = self._hex_normalizer(nodeid)
        logging.info('Waiting wakeup notification from node {0}'.format(nodeid))
        while True:
            try:
                self.uartapi.block_and_wait_byte(waittime=-1)
                frame = self.get_network_message()
                if frame:
                    if frame[2]['Command'] == "WAKE_UP_NOTIFICATION" and self._hex_normalizer(frame[1]) == nodeid:
                        logging.info('Received WAKE_UP_NOTIFICATION')
                        return True
            except (UartTimeoutError, ZwaveInterfaceError):
                self._send_ack()
                self.uartapi.flush()
                continue  #we want an endles loop


    ##############Utilities functions, PRIVATE#################
    def _create_payload(self, nodeid, command):
        """This will create the payload necessary for the next function, it takes nodeID and the command
        and return the SERIAL API payload with the serial api command code, nodeid, lenght of the command
        payload = -SerialAPIcode, -rxstatus, -nodeID, -lengthofCMD, -CMD """
        nodeid = self._hex_normalizer(nodeid)
        if isinstance(command, str):
            if command[0] + command[1] == '0x':
                command = BitStream(command)
            else:
                command = BitStream(hex=command)
        lenght = format(len(command.bytes), '02X')
        rxstatus = '00'
        serialpayload = ZWAVE_CMD_APPLICATIONCOMMANDHANDLER + rxstatus + nodeid + lenght + command.hex
        return serialpayload

    def _payload_to_frame(self, data_str):
        """ Create a Z-Wave frame from the string payload in parameter.
        Example: to send the hexadecimal 0x01 0x02 0x03 type the string
        '010203' in data_str parameter. The frame is composed by the following
        fields: -SOF, -LENGTH , -TYPE, -COMMAND, -PARAMETERS and -CRC. The data_str
        parameter correspond to the COMMAND and PARAMETERS fields. The TYPE of the
        frame is always a REQUEST (0x00) when speaking to the module (HOST->ZW).
        """
        frame = BitStream()
        frame.append('0x01')  # SOF
        frame.append('0x' + format(len(data_str) / 2 + 2, '02X'))  # LENGTH
        frame.append('0x00')  # TYPE
        frame.append('0x' + data_str)  # COMMAND and PARAMs
        frame.append('uint:8=' + str(self._calc_crc(frame)))  # CRC
        return frame

    def _unpack_dataframe(self, data):
        """ Interpret the incoming data frame.
        The <stream> parameter is a hex string typically
        created with binascii.b2a_hex().
        The function can handle more that one frame in the stream.
        The return is a dictionary
        """
        # Pre-treat the stream in case it is not in the correct format
        stream = self._sof_normalizer(data)
        frames = []

        while stream.pos != stream.len:
            # ACK, SOF ?
            acksof = stream.read('hex:8')
            if acksof == '06':
                # We exit the loop if the response is only an ACK
                continue

            if acksof == '01':
                dataframe = {}
                lenght = stream.read('uint:8')

                cmdtype = stream.read('hex:8')
                serialapi_cmd = stream.read('hex:8')
                params = stream.read('hex:' + str(8 * (lenght - 3)))
                # Note: The verification the CRC must be realized on the microcontroller
                checksum = stream.read('hex:8')
                rawdata = str(acksof + "{:02x}".format(lenght) + cmdtype + serialapi_cmd + params + checksum)
                if cmdtype == 0x00:
                    dataframe['TYPE'] = 'REQUEST'
                else:
                    dataframe['TYPE'] = 'RESPONSE'

                dataframe['COMMAND'] = serialapi_cmd
                dataframe['PARAMETERS'] = params
                dataframe['RAW'] = rawdata
                frames.append(dataframe)

        if len(frames) == 1:
            return frames[0]
        else:
            return frames

    @staticmethod
    def _unpack_application_command(params):
        """ Serial API: REQ | 0x04 | ...
        The function returns:
        - the NodeID of the node which send this information and the
        - Command Class Payload contained in the data frame
        """
        nodeid, cmdclass = None, None
        stream = BitStream('0x' + params)
        while stream.pos != stream.len:
            rxstatus = stream.read('hex:8') #08 is broadcast, 01 is single cast
            if rxstatus == '08': # we ignore the broadcasts
                return
            nodeid = stream.read('uint:8')
            cmdlength = stream.read('uint:8')
            cmdclass = stream.read('hex:' + str(8 * cmdlength))
        return nodeid, cmdclass

    @staticmethod
    def _unpack_command_class(cmdclass):
        """This function returns the:
        - The Command Class (the group of the commands)
        - The Command (the command itself)
        - The Data of the command."""

        stream = BitStream('0x' + cmdclass)
        cmdclass = stream.read('hex:8')
        command = stream.read('hex:8')
        data = stream.read('hex:' + str(stream.len - stream.pos))
        return cmdclass, command, data

    def _get_command_name(self, cmdclass, command):
        """ This function looks in the XML file to find the name
        of the Command Class and the Command. If the command is not found,
        the function returns the numerical value as passed in the input."""
        cmdclassname = "0x" + str(cmdclass)
        cmdname = "0x" + str(command)

        try:
            cmdclasselem = self._commandclasses.findall("cmd_class[@key='0x" + str(cmdclass) + "']")[-1]
            cmdelem = cmdclasselem.find("cmd[@key='0x" + str(command) + "']")
            cmdclassname = cmdclasselem.attrib.get('name')
            cmdname = cmdelem.attrib.get('name')
        except Exception:
            pass
        return cmdclassname, cmdname

    def _interpret_command_class(self, cmdclass, command, data):
        """ The Command Class can be interpreted with this function.
        The function returns a dictionnary with at least the keys
        Command Class and Command in an human readable form. If the
        interpretation of the Command Class is implemented, the function
        complete the dictionnary with the values contained in the command.
        """
        cmdclassname, cmdname = self._get_command_name(cmdclass, command)
        if data:
            stream = BitStream('0x' + data)
            dico = {'Command Class': cmdclassname, 'Command': cmdname}
        else:
            dico = {'Command Class': cmdclassname, 'Command': cmdname}
            return dico


        if cmdclassname == 'COMMAND_CLASS_BASIC' and \
                cmdname == 'BASIC_SET':
            dico['Value'] = stream.read('hex:8').upper()

        elif cmdclassname == 'COMMAND_CLASS_SENSOR_MULTILEVEL' and \
                cmdname == 'SENSOR_MULTILEVEL_REPORT':
            try:
                sensortype = stream.read('hex:8')
                precision = stream.read('uint:3')
                scale = "{:02X}".format(stream.read('uint:2'))
                size = stream.read('uint:3')
                sensorvalue = stream.read('int:' + str(8 * size))
            except (ReadError, InterpretError, AttributeError):
                logging.warning('Corrupted packet with correct CRC...skipping...')
                return False

            if precision != 0:
                svalue = ('%3.' + str(precision) + 'f') % (float(sensorvalue) / (pow(10, precision)))
            else:
                svalue = '%i' % sensorvalue
            sensortypeelem = self._multilevelsensors.find("type[@key='0x" + str(sensortype.upper()) + "']")
            if sensortypeelem is None:  # fail safe in case something went terribly wrong and the key didnt match to xml
                return False
            sensorscaleelem = sensortypeelem.find("scale[@key='0x" + scale + "']")
            if sensorscaleelem is None:  # fail safe in case something went terribly wrong and the key didnt match to xml
                return False
            dico['Sensor Type'] = sensortypeelem.attrib.get('name')
            dico['Scale'] = sensorscaleelem.attrib.get('name')
            dico['Unity'] = sensorscaleelem.attrib.get('unity')
            dico['Value'] = svalue

        elif cmdclassname == 'COMMAND_CLASS_BATTERY' and \
                cmdname == 'BATTERY_REPORT':
            dico['Sensor Type'] = 'Battery Level'
            dico['Value'] = stream.read('uint:8')
            dico['Scale'] = "Percentage"
            dico['Unity'] = "%"

        elif cmdclassname == 'COMMAND_CLASS_CONFIGURATION' and \
                cmdname == 'CONFIGURATION_REPORT':
            paramnum = stream.read('uint:8')
            paramconf = stream.read('bin:8')
            size = int(paramconf[-3:], 2)
            try:
                confvalue = stream.read('uint:' + str(8 * size))
            except (ReadError, InterpretError, AttributeError):
                # Should not reach it, just in case
                logging.warning('Corrupted packet with correct CRC...skipping...')
                return False
            dico['Parameter number'] = paramnum
            dico['Parameter configuration'] = paramconf
            dico['Configuration value'] = confvalue

        elif cmdclassname == 'COMMAND_CLASS_SENSOR_BINARY' and \
                cmdname == 'SENSOR_BINARY_REPORT':
            presencevalue = stream.read('hex:8').upper()
            if presencevalue == 'FF':
                dico['Sensor Type'] = 'PIR'
                dico['Value'] = True
            elif presencevalue == '00':
                dico['Sensor Type'] = 'PIR'
                dico['Value'] = False
        elif cmdclassname == 'COMMAND_CLASS_THERMOSTAT_SETPOINT' and \
                cmdname == 'THERMOSTAT_SETPOINT_REPORT':
            sensortype = stream.read('hex:8')
            precision = stream.read('uint:3')
            scale = "{:02X}".format(stream.read('uint:2'))
            size = stream.read('uint:3')
            sensorvalue = stream.read('int:' + str(8 * size))
            svalue = ('%3.' + str(precision) + 'f') % (float(sensorvalue) / (pow(10, precision)))
            if sensortype == '01':
                dico['Sensor Type'] = 'Heating'
            dico['Unity'] = 'oC'
            dico['Value'] = svalue
        elif cmdclassname == 'COMMAND_CLASS_METER' and \
                 cmdname == 'METER_REPORT':
            ratetype = "{:02X}".format(stream.read('uint:3')) #consuming or generating energy...
            metertype = "{:02X}".format(stream.read('uint:5'))
            precision = stream.read('uint:3')
            scale = "{:02X}".format(stream.read('uint:2'))
            size = stream.read('uint:3')
            sensorvalue = stream.read('int:' + str(8 * size))
            svalue = ('%3.' + str(precision) + 'f') % (float(sensorvalue) / (pow(10, precision)))
            dico['Value'] = svalue
            if metertype == "01":
                dico['Meter Type'] = 'Electric meter'
                dico['Unity'] = 'kWh'

        # Whenever there is uninterpreted data in the frame,
        # the data is added in the dictionary in the 'Uninterpreted data' field
        if data and stream.len - stream.pos:
            field = stream.read('hex:' + str(stream.len - stream.pos))
            dico['Uninterpreted data'] = field.upper()
        return dico

    def gen_funcid(self):
        return self._hex_normalizer(random.randint(1,16))

    @staticmethod
    def _calc_crc(dataframe):
        dataframe.bytepos = 1  # the SOF is not calculated in the CRC
        checksum = 0xFF
        while dataframe.pos < dataframe.length:
            checksum ^= dataframe.read('uint:8')
        return checksum

    @staticmethod
    def _verify_serialapi_cmd(decodedresponse, serialapi_cmd):
        if not decodedresponse['COMMAND'] == serialapi_cmd:
            logging.error('Wrong Serial API Command reveived: {0}'.format(decodedresponse['COMMAND']))
            raise ZwaveInterfaceError

    @staticmethod
    def _verify_funcid(decodedresponse, funcid):
        if not decodedresponse['PARAMETERS'][:2] == funcid:
            logging.error('Wrong callback funcid received')
            raise ZwaveInterfaceError

    @staticmethod
    def _sof_normalizer(data):
        """Takes an SOF input in the format of string or bitstream and delivers a Bitstream iteam
        regardless of the input or string format"""
        if isinstance(data, str):
            stream = data
            if stream[0] == '\x01':
                # Stream in bytes format
                stream = BitStream(bytes=stream)
            elif stream[0] + stream[1] == '01':
                stream = data.replace(" ", "")  # Remove the spaces that may be inside frame
                stream = BitStream(hex=stream)
            elif stream[0] + stream[1] == '0x':
                stream = BitStream(stream)
            return stream
        elif isinstance(data, BitStream):
            data.pos = 0
            return data

    @staticmethod
    def _hex_normalizer(hexvalue):
        """Normalizer for various hex formats given to arguments"""
        if isinstance(hexvalue, int):
            hexvalue = format(hexvalue, '02x')
        elif isinstance(hexvalue, str):
            if hexvalue[0] + hexvalue[1] == '0x':
                hexvalue = hexvalue[2:]
        return hexvalue