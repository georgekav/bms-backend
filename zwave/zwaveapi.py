__author__ = 'Georgios Lilis'
import sys,os,logging
PROJECT_DIR = os.path.abspath(__file__ + "/../../../")
sys.path.append(os.path.join(PROJECT_DIR, 'config'))
from conf_zwave import ZWAVE_DESIRED_CONF
from zwave.zwaveinterface import ZwaveInterface, repetition_decorator, ZWAVE_CMD_SENDDATA

ZWAVE_CLASS_ASSOCIATIONV2 = '85'
ZWAVE_CLASS_CLIMATECONTROLSCHEDULE = '46'
ZWAVE_CLASS_SENSORMULTILEVELV2 = '31'
ZWAVE_CLASS_CONFIGURATION = '70'
ZWAVE_CLASS_BATTERY = '80'
ZWAVE_CLASS_THERMOSTATSETPOINT = '43'
ZWAVE_CLASS_CLOCK = '81'
ZWAVE_CLASS_WAKEUP = '84'

class AeotecApi(ZwaveInterface):
    def __init__(self, uartapi):
        super(AeotecApi, self).__init__(uartapi=uartapi)
        if not self.status:
            raise RuntimeError('Zwave interface not ready, check uart device status')

    @repetition_decorator
    def get_multisensor_values(self, nodeid, measure, syncbybutton=False, nodelistening=False):
        """This function will force read the Aeotec sensor value"""
        nodeid = self._hex_normalizer(nodeid)
        mestype = dict([('Air temperature', '01'), ('Luminance', '03'), ('Humidity', '05')])
        serialapi_cmd = ZWAVE_CMD_SENDDATA
        cmd_class = ZWAVE_CLASS_SENSORMULTILEVELV2
        cmd_get = '04'
        payload = cmd_class + cmd_get + mestype[measure] +'00'
        lencmd = format(len(payload) / 2, '02X')  # length in bytes...
        maincmd = serialapi_cmd + nodeid + lencmd + payload
        funcid = self.gen_funcid()
        txoptions = '25'
        msgtosend = maincmd + txoptions + funcid
        if not nodelistening:
            if syncbybutton:
                self.wait_button_press(nodeid)
            else:
                self.wait_wakeup_notification(nodeid)
        self._send_request(msgtosend)
        self._confirm_zwcmd_send(funcid)
        frame = self.get_network_message()
        if frame:
            if frame[2]['Command'] == "SENSOR_MULTILEVEL_REPORT" and self._hex_normalizer(frame[1]) == nodeid:
                return frame

    @repetition_decorator
    def set_configuration(self, nodeid, param):
        """This functions set the configuration of the Aeotec sensor"""
        nodeid = self._hex_normalizer(nodeid)
        serialapi_cmd = ZWAVE_CMD_SENDDATA
        cmd_class = ZWAVE_CLASS_CONFIGURATION
        setconfcode = '04'
        payload = cmd_class + setconfcode + param[0] + param[1] + param[2]
        lencmd = format(len(payload) / 2, '02X')  # length in bytes...
        maincmd = serialapi_cmd + nodeid + lencmd + payload
        funcid = self.gen_funcid()
        txoptions = '25'
        message = maincmd + txoptions + funcid
        self._send_request(message)
        self._confirm_zwcmd_send(funcid=funcid)
        logging.info('Configuration of node {0} with param {1} DONE'.format(nodeid, param))
        return True

    @repetition_decorator
    def get_configuration(self, nodeid, paramnum):
        """This function gets the configuration of the Aeotec Sensor"""
        nodeid = self._hex_normalizer(nodeid)
        serialapi_cmd = ZWAVE_CMD_SENDDATA
        cmd_class = ZWAVE_CLASS_CONFIGURATION
        paramnum = self._hex_normalizer(paramnum)
        readconfcode = '05'
        payload = cmd_class + readconfcode + paramnum
        lencmd = format(len(payload) / 2, '02X')  # length in bytes...
        maincmd = serialapi_cmd + nodeid + lencmd + payload
        funcid = self.gen_funcid()
        txoptions = '25'
        msgtosend = maincmd + txoptions + funcid
        self._send_request(msgtosend)
        self._confirm_zwcmd_send(funcid=funcid)
        decodedresponse = self._unpack_dataframe(self._receive_response())
        logging.info('Configuration of node {0}: {1}'.format(nodeid, decodedresponse['PARAMETERS']))
        return True

    @repetition_decorator
    def set_association(self, nodeid, groupid, assosiatednodelist):
        """This function adds to the nodeid's groupid all the nodes from associatednodelist so that they can get updates.
        It should be applied to the sensors and not primary controller. aka "set_association(modulesdiscovered, 1, [1])" """
        nodeid = self._hex_normalizer(nodeid)
        serialapi_cmd = ZWAVE_CMD_SENDDATA
        cmd_class = ZWAVE_CLASS_ASSOCIATIONV2
        set_cmd = '01'
        groupid = self._hex_normalizer(groupid)
        nodes=''
        if not isinstance(assosiatednodelist, list): assosiatednodelist = [assosiatednodelist]
        for n in assosiatednodelist:
            nodes += self._hex_normalizer(n)
        txoptions = '25'
        funcid = self.gen_funcid()
        maincmd = cmd_class + set_cmd + groupid + nodes
        datalenght = format(len(maincmd) / 2, '02X')
        message = serialapi_cmd+nodeid+datalenght+maincmd+txoptions+funcid
        self._send_request(message)
        self._confirm_zwcmd_send(funcid)
        logging.info('Assossiation of node {0} with group {1} and nodes {2} DONE'.format(nodeid, groupid, assosiatednodelist))
        return True



    def configure_nodes(self, nodelist, sensconflist=ZWAVE_DESIRED_CONF, syncbybutton=True,
                        nodelistening=False):
        """nodelist the nodes to be configured, Paramnum the parameter num to be configured in hex string
        size the size of configuration values in hexstr, confvalue the value itself
        if sync we wait for button press instead of wake packet
        if nodelistening then no wait at all"""
        for nodeid in nodelist:  # NodeID is in int so we change to hex for the zwave interface functions.
            for i, senconf in enumerate(sensconflist):
                nodeid = self._hex_normalizer(nodeid)
                paramnum = self._hex_normalizer(senconf[0])
                size = self._hex_normalizer(senconf[1])
                confvalue = self._hex_normalizer(senconf[2])
                parameters = (paramnum, size, confvalue)
                if not nodelistening:
                    if syncbybutton:
                        self.wait_button_press(nodeid)
                    else:
                        self.wait_wakeup_notification(nodeid)
                ret =  self.set_configuration(nodeid, parameters)
                if not ret:
                    logging.error('Configuration for node {0} FAILED'.format(nodeid))


class DanfossApi(ZwaveInterface):
    """This applies to all the following functions:
    sync boolean states if the user must push the button to initialize a sync transfer or to wait for
    the wake up packet given at the defined interval. nodelistening is useful when you need a queue of commands,
    in this case no need to wait a new wake packet, the first is enough."""

    def __init__(self, uartapi):
        super(DanfossApi, self).__init__(uartapi=uartapi)
        if not self.status:
            raise RuntimeError('Zwave interface not ready, check uart device status')

    def get_all_async_reports(self):
        """Endless loop reading all the reports from the valve"""
        logging.info('Infinite loop collecting incomming packets')
        while True:
            self.uartapi.block_and_wait_byte(-1)
            ret = self.get_network_message()
            logging.info(ret)

    @repetition_decorator
    def set_setpoint(self, nodeid, temperature, sync=True, nodelistening=False):
        """Set the master temperature setpoint of the valve"""
        if not nodelistening:
            if sync:
                self.wait_button_press(nodeid)
            else:
                self.wait_wakeup_notification(nodeid)
        nodeid = self._hex_normalizer(nodeid)
        serialapi_cmd = ZWAVE_CMD_SENDDATA
        cmd_class = ZWAVE_CLASS_THERMOSTATSETPOINT
        cmd_set = '01'
        sen_type = '01' # Heating
        conf = '42'  # 3 bit presision 2bit scale 3bit size >> presition =2 scale=0 size=2
        setting = temperature
        enc_setting = format(int(setting * pow(10, 2)), '04X')
        funcid = self.gen_funcid()
        payload = cmd_class + cmd_set + sen_type + conf + enc_setting
        lencmd = format(len(payload) / 2, '02X')  # length in bytes...
        txoptions = '25'
        msgtosend = serialapi_cmd + nodeid + lencmd +  payload + txoptions + funcid
        self._send_request(msgtosend)
        self._confirm_zwcmd_send(funcid)
        logging.info('SetPoint of node {0} with temperature {1} DONE'.format(nodeid, temperature))
        return True

    @repetition_decorator
    def get_setpoint(self, nodeid, sync=True, nodelistening=False):
        """Get the current master set point setting"""
        if not nodelistening:
            if sync:
                self.wait_button_press(nodeid)
            else:
                self.wait_wakeup_notification(nodeid)
        nodeid = self._hex_normalizer(nodeid)
        serialapi_cmd = ZWAVE_CMD_SENDDATA
        cmd_class = ZWAVE_CLASS_THERMOSTATSETPOINT
        cmd_get = '02'
        sen_type = '01'  # Heating
        payload = cmd_class + cmd_get + sen_type
        lencmd = format(len(payload) / 2, '02X')  # length in bytes...
        txoptions = '25'
        funcid = self.gen_funcid()
        msgtosend = serialapi_cmd + nodeid + lencmd + payload + txoptions + funcid
        self._send_request(msgtosend)
        self._confirm_zwcmd_send(funcid)
        (timestamp, nodeid, info) = self.get_network_message()
        logging.info('Received from {0} the following set point {1} {2}'.format(nodeid, info['Value'], info['Unity']))
        return timestamp, info

    @repetition_decorator
    def get_battery(self, nodeid, sync=True, nodelistening=False):
        """Just read the battery status"""
        logging.info('Get Battery Report')
        if not nodelistening:
            if sync:
                self.wait_button_press(nodeid)
            else:
                self.wait_wakeup_notification(nodeid)
        nodeid = self._hex_normalizer(nodeid)
        serialapi_cmd = ZWAVE_CMD_SENDDATA
        cmd_class = ZWAVE_CLASS_BATTERY
        cmd_get = '02'
        payload = cmd_class + cmd_get
        lencmd = format(len(payload) / 2, '02X')  # length in bytes...
        txoptions = '25'
        funcid = self.gen_funcid()
        msgtosend = serialapi_cmd + nodeid + lencmd + payload + txoptions + funcid
        self._send_request(msgtosend)
        self._confirm_zwcmd_send(funcid)
        (timestamp, nodeid, info) = self.get_network_message()
        logging.info('Received from {0} the following battery level {1} {2}'.format(nodeid, info['Value'], info['Unity']))
        return timestamp, info

    @repetition_decorator
    def set_clock(self, nodeid, (weekday, hour, minute), sync=True, nodelistening=False):
        """clksetting is a tuple with the fields in int, (weekday, hour, minute)
        weekday int: 1 for monday... etc"""
        logging.info('Set clock')
        if not nodelistening:
            if sync:
                self.wait_button_press(nodeid)
            else:
                self.wait_wakeup_notification(nodeid)
        nodeid = self._hex_normalizer(nodeid)
        serialapi_cmd = ZWAVE_CMD_SENDDATA
        cmd_class = ZWAVE_CLASS_CLOCK
        cmd_set = '04'
        setting1 = weekday << 5 | hour
        setting2 = minute
        setting = format(setting1, '02X') + format(setting2, '02X')
        payload = cmd_class + cmd_set + setting
        lencmd = format(len(payload) / 2, '02X')  # length in bytes...
        txoptions = '25'
        funcid = self.gen_funcid()
        msgtosend = serialapi_cmd + nodeid + lencmd + payload + txoptions + funcid
        self._send_request(msgtosend)
        self._confirm_zwcmd_send(funcid)
        logging.info('Time of node {0} @ {1} {2}:{3} {1} DONE'.format(nodeid, weekday, hour, minute))
        return True

    @repetition_decorator
    def set_reporting_interval(self, nodeid, seconds, nodetoreport, sync=True, nodelistening=False):
        """Set every how many seconds the thermostat will report battery, setpoint and scedule, and at which nodeid"""
        logging.info('Set reporting interval')
        if not nodelistening:
            if sync:
                self.wait_button_press(nodeid)
            else:
                self.wait_wakeup_notification(nodeid)
        nodeid = self._hex_normalizer(nodeid)
        serialapi_cmd = ZWAVE_CMD_SENDDATA
        cmd_class = ZWAVE_CLASS_WAKEUP
        cmd_set = '04'
        setting1 = format(seconds, '06X')
        setting2 = format(nodetoreport, '02X')
        payload = cmd_class + cmd_set + setting1 + setting2
        lencmd = format(len(payload) / 2, '02X')  # length in bytes...
        txoptions = '25'
        funcid = self.gen_funcid()
        msgtosend = serialapi_cmd + nodeid + lencmd + payload + txoptions + funcid
        self._send_request(msgtosend)
        self._confirm_zwcmd_send(funcid)
        logging.info('Reporting interval of node {0} set to report to {1} DONE'.format(nodeid, nodetoreport))
        return True

    @repetition_decorator
    def set_schedule(self, nodeid, weekday, hourminutes, difftosetpoint, sync=True, nodelistening=False):
        """hourminute is a list of (hour,minute) and difftosetpoint is a list of +-correction to setpoint
        for the specified hour,minute. weekday is an int for the day starting on monday at which the scedule will apply.
        the list MUST be sorted in time and should be length of nine for the message to be accepted. For that reason
        if less than 9 we fill the packet.
        weekday int: 1 for monday... etc
        difftosetpoint a +- float to defined setpoint"""
        logging.info('Set scedule')
        if not nodelistening:
            if sync:
                self.wait_button_press(nodeid)
            else:
                self.wait_wakeup_notification(nodeid)
        nodeid = self._hex_normalizer(nodeid)
        switpointspacket = []
        if len(hourminutes) != len(difftosetpoint) or len(hourminutes) > 9 or len(difftosetpoint) > 9:
            return
        for i, hm in enumerate(hourminutes):
            if not hm:
                break  # None is given, return the schedule to defaults
            setting1 = format(hm[0], '02X') + format(hm[1], '02X')
            setting2 = format(int(difftosetpoint[i] * 10) & 0xFF, '02X')  # we need the complement of 2
            switpointspacket.append(setting1 + setting2)
        while len(switpointspacket) < 9:
            switpointspacket.append('00007F')  # To say that this xth field is unused.
        settingstring = ''.join(switpointspacket)
        serialapi_cmd = ZWAVE_CMD_SENDDATA
        cmd_class = ZWAVE_CLASS_CLIMATECONTROLSCHEDULE
        cmd_set = '01'
        weekday = format(weekday, '02X')        
        payload = cmd_class + cmd_set + weekday + settingstring
        lencmd = format(len(payload) / 2, '02X')  # length in bytes...
        txoptions = '25'
        funcid = self.gen_funcid()
        
        msgtosend = serialapi_cmd + nodeid + lencmd + payload + txoptions + funcid
        self._send_request(msgtosend)
        self._confirm_zwcmd_send(funcid)
        return True

class PrimaryControllerApi(ZwaveInterface):
    def __init__(self, uartapi):
        super(PrimaryControllerApi, self).__init__(uartapi=uartapi)
        if not self.status:
            raise RuntimeError('Zwave interface not ready, check uart device status')

    @repetition_decorator
    def reset_reprogram_prim(self):
        self.send_primary_hard_reset()
        while True: #reprogram controller
            self.remove_node()  #First we need to reset sensor stored homeid by removal...
            self.add_node()
            modulesdiscovered = self.get_node_list()
            logging.info(modulesdiscovered)
            logging.info('Nodes need reconfiguring according to requirements (time interval, associations etc)')


# #Example 1
# from uart import SerialInterface
# from conf_zwave import  ZWAVE_USB_PARAM
# import logging
#
# logging.root.setLevel(logging.INFO)
# serialapi = SerialInterface(usbconf=ZWAVE_USB_PARAM)
# ret = serialapi.connect()
#
# api = DanfossApi(uartapi=serialapi)
# api.get_all_async_reports()

# #Example 2
# from uart import SerialInterface
# from conf_zwave import  ZWAVE_USB_PARAM
# import logging
#
# logging.root.setLevel(logging.INFO)
# serialapi = SerialInterface(usbconf=ZWAVE_USB_PARAM)
# ret = serialapi.connect()
#
# api = DanfossApi(uartapi=serialapi)
# api.set_reporting_interval(2,240,1,sync=True, nodelistening=False)

# #Example 3
# from uart import SerialInterface
# from conf_zwave import  ZWAVE_USB_PARAM
# import logging
#
# logging.root.setLevel(logging.INFO)
# serialapi = SerialInterface(usbconf=ZWAVE_USB_PARAM)
# ret = serialapi.connect()
#
# api = DanfossApi(uartapi=serialapi)
# api.get_setpoint(3,sync=True)

# #Example 4
# from uart import SerialInterface
# from conf_zwave import  ZWAVE_USB_PARAM
# import logging
#
# logging.root.setLevel(logging.INFO)
# serialapi = SerialInterface(usbconf=ZWAVE_USB_PARAM)
# ret = serialapi.connect()
#
# api = AeotecApi(uartapi=serialapi)
# api.send_primary_soft_reset()
# modulesdiscovered = api.get_node_list()
# if 1 in modulesdiscovered: modulesdiscovered.remove(1) #Remove the static controller
# api.configure_nodes(modulesdiscovered,nodelistening=True)
# for n in modulesdiscovered:
#     api.set_association(n, 1, [1])

# #Example 5
# from uart import SerialInterface
# from conf_zwave import  ZWAVE_USB_PARAM
# import logging, time
#
# logging.root.setLevel(logging.INFO)
# serialapi = SerialInterface(usbconf=ZWAVE_USB_PARAM)
# ret = serialapi.connect()
#
# api = DanfossApi(uartapi=serialapi)
# while True: #reprogram controller
#     api.remove_node()
#     api.add_node()
#     modulesdiscovered = api.get_node_list()
#     print modulesdiscovered

# #Example 6
# from uart import SerialInterface
# from conf_zwave import  ZWAVE_USB_PARAM
# import logging
#
# logging.root.setLevel(logging.INFO)
# serialapi = SerialInterface(usbconf=ZWAVE_USB_PARAM)
# ret = serialapi.connect()
#
# api = PrimaryControllerApi(uartapi=serialapi)
# api.set_learn_mode()