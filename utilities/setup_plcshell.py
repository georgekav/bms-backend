import sys

from cx_Freeze import setup, Executable

includefiles = []
includes = ['os', 'socket', 'zmq.backend.cython', 'define']
excludes = ['_gtkagg', '_tkagg', 'bsddb', 'curses', 'email', 'pywin.debugger',
            'pywin.debugger.dbgcon', 'pywin.dialogs', 'tcl',
            'Tkconstants', 'Tkinter', 'scipy', 'numpy', '_ssl', 'unittest', 'xml', 'unicodedata', '_hashlib', 'gevent',
            'tornado', 'pyreadline', 'nose', 'logging', 'json', 'doctest', 'urllib',
            'urllib']
packages = []
sys.path.append('../')
sys.path.append('../../libs/')
sys.path.append('../../libs/zmq')
shellexedir = 'C:/work/elab-bms/builds/PLC shell'
setup(
    name="PLC shell",
    version="0.1",
    description="A PLC cmd shell",
    options={'build_exe': {'build_exe': shellexedir, 'excludes': excludes, 'includes': includes, 'packages': packages,
                           'include_files': includefiles}},
    executables=[Executable(
        script="plcshell.py",
        initScript=None,
        compress=True,
        copyDependentFiles=True,
        appendScriptToExe=False,
        appendScriptToLibrary=False,
    )])