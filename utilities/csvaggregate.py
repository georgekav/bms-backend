__author__ = 'Georgios Lilis'
"""This script will clean up the csv files to only have a maximum defined amplitude resolution"""
import logging
import copy
from db.csvinterface import CsvInterface,CsvInterfaceError

def csv_time_aggregator(filelist, filetags, fieldlist, inputdir, outputdir, mainresolution=300, highresolution=None):
    """Takes a csv file list and aggragates the data according to the requested resolution
    First open the filelist with read access. Read all the rows to the memory.
    Then closes the files and open them again in write mode.
    And of course in the mean time it aggregates the data.
    .....NOT TESTED FOR LONG....
    .....NOT TESTED FOR LONG....
    .....NOT TESTED FOR LONG....
    .....NOT TESTED FOR LONG....
    .....NOT TESTED FOR LONG...."""
    if len(filelist) != len(fieldlist):
        logging.error('File list length is not equal with fieldnamelist length')
    csvreader = CsvInterface(mode='r', filelist=filelist, currentdir=inputdir)
    allrows = csvreader.read_from_csv()  # We read in the memory, now we can close the files
    csvreader.close_all_csv()  # csv files are closed.
    rowstowrite = []
    if not highresolution:
        highresolution = ((3 * 24 * 3600, mainresolution / 2), (24 * 3600, mainresolution / 5),
                          (3600, mainresolution / 10))  # increase resolution for last samples
    # x is the number of file in the filelist given in the function
    for filenum in range(len(filelist)):
        try:
            fieldnames = fieldlist[filenum]
            filerows = allrows[filenum]  # filerows is one file's rows
            if len(filerows) == 0:  # empty file, nothing to do, e.g. PIR10
                continue
            currenttime = float(filerows[0]['Time'])
            endtime = float(filerows[-1]['Time'])
            resolution = mainresolution
            # Different high resolution steps for the latest values
            firstcheckpoint = endtime - highresolution[0][0]
            secondcheckpoint = endtime - highresolution[1][0]
            thirdcheckpoint = endtime - highresolution[2][0]
            average = {}
            for field in fieldnames:
                average[field] = 0  # initialize
            count = 0
            for i in range(len(filerows)):  # Accessing all file's values
                if float(filerows[i]['Time']) < currenttime + resolution:
                    count += 1
                    for field in fieldnames:
                        average[field] += float(filerows[i][field])
                else:
                    for field in fieldnames:
                        average[field] = round(average[field] / count, 3)
                    row = dict([(filetags[filenum], average)])
                    rowstowrite.append(copy.deepcopy(row))

                    count = 1  # we count the current sample
                    for field in fieldnames:
                        average[field] = float(
                            filerows[i][field])  # reinitialize the average,we count the current sample
                    currenttime = float(filerows[i]['Time'])
                    # correct the resolution as we approach the end time, they should be
                    # sorted because resolution var does not recover
                    if firstcheckpoint < currenttime <= secondcheckpoint and resolution != highresolution[0][1]:
                        resolution = highresolution[0][1]
                    elif secondcheckpoint < currenttime <= thirdcheckpoint and resolution != highresolution[1][1]:
                        resolution = highresolution[1][1]
                    elif thirdcheckpoint < currenttime and resolution != highresolution[2][1]:
                        resolution = highresolution[2][1]
        except KeyError as e:
            logging.exception("Key" + repr(e) + "not found. Aggregation failed")
            raise KeyError("Key not found: " + str(e))

    csvwriter = CsvInterface(mode='w', filelist=filelist, filetags=filetags, fieldnameslist=fieldlist,
                           currentdir=outputdir)
    try:
        csvwriter.write_to_csv(rowstowrite)
    except CsvInterfaceError:
        logging.exception('Not written to csv file')



# Valuekey is the header key of the csv on which the triggervalue will apply
def csv_amplitude_aggregator(filelist, filetags, fieldlist, inputdir, outputdir, valuekey, triggervalue):
    """Validated that it works!"""
    if len(filelist) != len(fieldlist):
        logging.error('Input lists lenght do not match')

    # file is the number of file in the filelist given in the function
    for index, filename in enumerate(filelist):
        try:
            csvreader = CsvInterface(mode='r', filelist=[filelist[index]], filetags=[filetags[index]], fieldnameslist=[fieldlist[index]], currentdir=inputdir)
            file_rows = csvreader.read_from_csv()[0] #Return a list of 1 element, we take directly that
            print("Read file {0} with rows {1}".format(filelist[index], len(file_rows)))
            csvreader.close_all_csv()  # csv files are closed.
            currentvalue = {}
            # Initialize and write first line
            rowstowrite = []
            for field in fieldlist[index]:
                currentvalue[field] = float(file_rows[0][field])  # initialize
            row = dict([(filetags[index], currentvalue)])
            rowstowrite.append(copy.deepcopy(row))
            skipcounter = 0
            for row in file_rows:  # Accessing all file's values
                if abs(float(row[valuekey]) - float(currentvalue[valuekey])) < triggervalue:
                    # No trigger, no change, skip this line
                    skipcounter += 1
                    continue
                else:
                    for field in fieldlist[index]:
                        currentvalue[field] = row[field]
                    row = dict([(filetags[index], currentvalue)])
                    rowstowrite.append(copy.deepcopy(row))
            csvwriter = CsvInterface(mode='w', filelist=[filelist[index]], filetags=[filetags[index]], fieldnameslist=[fieldlist[index]],
                           currentdir=outputdir)
            try:
                csvwriter.write_to_csv(rowstowrite)
            except CsvInterfaceError:
                logging.exception('Not written to csv file')
            print '{0}, aggregated: {1} records'.format(filelist[index], skipcounter)
        except KeyError as e:
            logging.exception("Key" + repr(e) + "not found. Aggregation failed")
            raise KeyError("Key not found: " + str(e))

if __name__ == '__main__':
    from conf_db import DB_STORE_DIR
    from os import listdir, makedirs
    from os.path import isfile, join, exists
    files_in_measures = [f for f in listdir(DB_STORE_DIR) if isfile(join(DB_STORE_DIR, f))]

    filelist = []
    fieldlist = []
    filetags = []

    for filename in files_in_measures:
        filelist.append(filename)
        filetags.append(filename[:-4])
        fieldlist.append(('Time', 'Value'))

    if not exists(join(DB_STORE_DIR,"aggregated")):
        makedirs(join(DB_STORE_DIR,"aggregated"))


    csv_amplitude_aggregator(filelist, filetags, fieldlist, DB_STORE_DIR, join(DB_STORE_DIR,"aggregated"),  valuekey='Value', triggervalue=0.0001)
