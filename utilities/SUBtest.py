__author__ = 'Georgios Lilis'
import zmq
context = zmq.Context()
#----------------------PLC-----------------------#
PLC_DAEMON_IP = '128.178.19.240'
PLC_DAEMON_PORT = '50200'
PLC_DAEMON_PORT_PUB = '50250'

#----------------------ZWAVE---------------------#
ZWAVE_DAEMON_IP = '128.178.19.240'
ZWAVE_DAEMON_PORT = '50300'
ZWAVE_DAEMON_PORT_PUB = '50350'

#----------------------TCP-----------------------#
#This should be the same with django server at the moment since they share the sqlite db
TCP_DAEMON_IP_PUB = '128.178.19.240'
TCP_DAEMON_PORT_PUB = '51050'

#----------------------LOCATION-----------------------#
LOC_DAEMON_IP_PUB = '128.178.19.163'
LOC_DAEMON_PORT_PUB = '51150'

#----------------------RFID-----------------------#
RFID_DAEMON_IP_PUB = '128.178.19.163'
RFID_DAEMON_PORT_PUB = '51250'

#----------------------vMID-----------------------#
VMID_DAEMON_IP_PUB = '128.178.19.163'
VMID_DAEMON_PORT_PUB = '51450'

SUBsocket = context.socket(zmq.SUB)
SUBsocket.connect('tcp://' + ZWAVE_DAEMON_IP + ':' + ZWAVE_DAEMON_PORT_PUB)
SUBsocket.connect('tcp://' + PLC_DAEMON_IP + ':' + PLC_DAEMON_PORT_PUB)
SUBsocket.connect('tcp://' + TCP_DAEMON_IP_PUB + ':' + TCP_DAEMON_PORT_PUB)
SUBsocket.connect('tcp://' + LOC_DAEMON_IP_PUB + ':' + LOC_DAEMON_PORT_PUB)
SUBsocket.connect('tcp://' + RFID_DAEMON_IP_PUB + ':' + RFID_DAEMON_PORT_PUB)
SUBsocket.connect('tcp://' + VMID_DAEMON_IP_PUB + ':' + VMID_DAEMON_PORT_PUB)

SUBsocket.setsockopt(zmq.SUBSCRIBE, '')

print 'Start the test of PUB/SUB modules'
while True:
    print SUBsocket.recv_json()
