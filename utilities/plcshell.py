__author__ = 'Georgios Lilis'
import zmq
from zmq import ZMQError



if __name__ == "__main__":
    context = zmq.Context()
    zmqsocket = context.socket(zmq.REQ)
    zmqsocket.connect('tcp://' + '128.178.19.163' + ':'+'50200')
    zmqsocket.setsockopt(zmq.LINGER, 2000)
    poller = zmq.Poller()
    poller.register(zmqsocket, zmq.POLLIN)

    try:
        while True:
            action = tuple(x.strip() for x in raw_input('Give: plcid, action, [auxiliary]   ').split(','))
            if len(action) == 3:
                if action[1].upper() != 'RTTEST':
                    zmqsocket.send_json((int(action[0]), action[1].upper(), int(action[2])))
                else:
                    zmqsocket.send_json((int(action[0]), action[1].upper(), action[2]))
            elif action[0] and action[1]:
                zmqsocket.send_json((int(action[0]), action[1].upper()))
            else:
                continue
            socks = dict(poller.poll(timeout=5000))
            if socks.get(zmqsocket) == zmq.POLLIN:
                ret = zmqsocket.recv_json()
                print ret
    except ZMQError:
        print "PLC daemon probably not running on remote, exiting..."