__author__ = 'Georgios Lilis'
from db.csvinterface import CsvInterface
from conf_db import DB_STORE_DIR

def csv_trancate(filelist, filetags, fieldlist, inputdir, outputdir, time_to_start_datetime):
    #allrows and filetags are synchronized as indexes
    for index, filename in enumerate(filelist):
        csvreader = CsvInterface(mode='r', filelist=[filelist[index]], filetags=[filetags[index]], fieldnameslist=[fieldlist[index]], currentdir=inputdir)
        file_rows = csvreader.read_from_csv()[0] #Return a list of 1 element, we take directly that
        print("Read file {0} with rows {1}".format(filelist[index], len(file_rows)))
        csvreader.close_all_csv()
        csvwriter = CsvInterface(mode='w', filelist=[filelist[index]], filetags=[filetags[index]], fieldnameslist=[fieldlist[index]], currentdir=outputdir)

        rows_to_save = []
        for row in file_rows:
            row_time = float(row['Time'])
            if row_time >= time_to_start:
                row_to_write = dict([(filetags[index], row)])
                rows_to_save.append(row_to_write)
        csvwriter.write_to_csv(rows_to_save)
        print("Wrote {0} lines in the file {1} starting from {2}".format(len(rows_to_save), filelist[index], time_to_start_datetime))


if __name__ == '__main__':

    import time
    from datetime import datetime
    from os import listdir, makedirs
    from os.path import isfile, join, exists
    files_in_measures = [f for f in listdir(DB_STORE_DIR) if isfile(join(DB_STORE_DIR, f))]

    filelist = []
    fieldlist = []
    filetags = []

    for filename in files_in_measures:
        filelist.append(filename)
        filetags.append(filename[:-4])
        fieldlist.append(('Time', 'Value'))


    time_to_start = time.time() - 86400 * 7 #day * 7 = week
    time_to_start_datetime = datetime.fromtimestamp(time_to_start).strftime('%Y-%m-%d %H:%M:%S')

    if not exists(join(DB_STORE_DIR,"truncated")):
        makedirs(join(DB_STORE_DIR,"truncated"))



    csv_trancate(filelist, filetags, fieldlist, DB_STORE_DIR,join(DB_STORE_DIR,"truncated"), time_to_start_datetime)