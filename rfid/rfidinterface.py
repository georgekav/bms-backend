from __future__ import division
__author__ = 'Georgios Lilis'
from conf_rfid import RFID_USB_PARAM_INIT, RFID_USB_PARAM_HS, ECHO_TRIES, CALIBRATION_3_3_V
from uart import UartTimeoutError, SerialInterface
from bitstring import ConstBitStream
from time import sleep
import logging


class RfidReaderInterface(object):
    """This class is necessary for handling all the communication with the CR95HF transceiver, echo, change of protocol,
     baud rate, etc..."""

    def __init__(self, uartapi):
        self.uartapi = uartapi
        if self.uartapi.device:
            self.status = True
        else:
            self.status = False

    def set_baud(self, rate, perform_echo = True):
        #rate in bps
        rate_int = int(round((13560000.0/rate - 2)/2,0))
        actual_rate = int(13.56/(2*rate_int+2)*1000000)
        if actual_rate != RFID_USB_PARAM_HS.BaudRate:
            logging.debug('The actual rate to be set is different from requested, baud rate did not change')
            return False
        else:
            rate_hex = '0A01{:02X}'.format(rate_int)
            self.uartapi.write(ConstBitStream(hex=rate_hex).bytes)
            logging.debug('Changing baud rate to {0}bps'.format(actual_rate))

            #Change our UART speed
            self.uartapi.disconnect()
            self.uartapi = SerialInterface(usbconf=RFID_USB_PARAM_HS)
            self.uartapi.connect()
            if perform_echo:
                self.echo_check()

    def echo_check(self):
        tries = 0
        while True:
            self.uartapi.write(ConstBitStream(hex='55').bytes)
            try:
                ret = self.uartapi.read(1)
                if ConstBitStream(bytes=ret).int == ConstBitStream(hex='55').int:
                    logging.debug('RFID echo success')
                    return True
            except UartTimeoutError:
                logging.debug('RFID  echo failed')
            tries += 1
            if tries > ECHO_TRIES:
                return False
            elif tries > ECHO_TRIES/2:
                logging.debug('Attempting echo at the HS baud rate')
                self.set_baud(RFID_USB_PARAM_HS.BaudRate, perform_echo=False)

    def protocol_select(self, pro_type):
        if pro_type == 'ISO15693':
            pro_select_code = ConstBitStream(hex='02020109').bytes
            pro_config_code = ConstBitStream(hex='0904680101D0').bytes
        elif pro_type == 'ISO14443A':
            pro_select_code = ConstBitStream(hex='02020200').bytes
            pro_config_code = ConstBitStream(hex='0904680101D0').bytes
        else:
            logging.error('Protocol {0} not supported'.format(pro_type))
            return False

        success = False
        while not success:
            self.uartapi.write(pro_select_code)
            ret = self.uartapi.read(2)
            if ConstBitStream(bytes=ret).int == ConstBitStream(hex='0000').int: success=True

        success = False
        while not success:
            self.uartapi.write(pro_config_code)
            ret = self.uartapi.read(2)
            if ConstBitStream(bytes=ret).int == ConstBitStream(hex='0000').int: success=True

        return True

    def hibernate(self, time_to_sleep):
        self.uartapi.write(ConstBitStream(hex='070E0804000400180000000000000000').bytes) #hibernate
        sleep(time_to_sleep)

        #Wakeup sequence

        self.uartapi.write(ConstBitStream(hex='0000').bytes) #wake-up
        sleep(0.01) #wait to come up

        #Connect with the default baud
        self.uartapi.disconnect()
        self.uartapi = SerialInterface(usbconf=RFID_USB_PARAM_INIT)
        self.uartapi.connect()

        #Change to high speed
        ret = self.set_baud(RFID_USB_PARAM_HS.BaudRate)
        if ret:
            #Change our speed
            self.uartapi.disconnect()
            self.uartapi = SerialInterface(usbconf=RFID_USB_PARAM_HS)
            self.serialapi.connect()
            self.echo_check()

    def calibrate(self):
        #Calibration for 3.3V
        self.uartapi.write(ConstBitStream(hex=CALIBRATION_3_3_V).bytes)
        sleep(0.5)
        self.uartapi.flush()
        logging.debug('Calibration finnished')