__author__ = 'Georgios Lilis'
from conf_rfid import TAG_LEFT_MAX, ECHO_TRIES
from rfid.rfidinterface import RfidReaderInterface
from bitstring import ConstBitStream
from time import sleep
import logging
import sys
from uart import UartTimeoutError



class RfidTagApi(RfidReaderInterface):
    def __init__(self, uartapi):
        super(RfidTagApi, self).__init__(uartapi=uartapi)
        if not self.status:
            raise RuntimeError('RFID interface not ready, check uart device status')
        self.uartapi_timeout_counter = 0

    def read_tag(self, pro_type):
        try:
            #Select correct protocol
            ret = self.protocol_select(pro_type=pro_type)
            if not ret:
                logging.error('Error setting the RFID protocol {0}'.format(pro_type))

            #Do the reading
            tag_left_counter = 0
            while True:
                if pro_type == 'ISO15693':
                    self.uartapi.write(ConstBitStream(hex='0403260100').bytes)
                    sleep(0.03)
                    read_success = self.uartapi.read(2) # XX LEN
                    if ConstBitStream(bytes=read_success[0]) == ConstBitStream(hex='80'):
                        read_len = ConstBitStream(bytes=read_success[1]).int
                        if read_len > 0:
                            read_data = self.uartapi.read(read_len)
                            logging.debug('RAW ISO15693 {0} {1}'.format(read_len,ConstBitStream(bytes=read_success+read_data).hex))
                            read_colision = ConstBitStream(bytes=read_data[-1])
                            if not (read_colision[-2:].uint & 0b01 or read_colision[-2:].uint & 0b10):
                            #else CRC failed or collision, bitwise check if true of bit 1 (CRC fail) OR bit 0 (collision)
                                rfid = ''
                                for id_byte in read_data[9:1:-1]:
                                    rfid += id_byte
                                rfid = ConstBitStream(bytes=rfid).hex
                                self.uartapi.flush()
                                return rfid #Instant return if detection
                    elif ConstBitStream(bytes=read_success[0]) == ConstBitStream(hex='8E') or \
                                    ConstBitStream(bytes=read_success[0]) == ConstBitStream(hex='88'):
                        logging.debug('Broken frame, something in the field')
                        tag_left_counter = 0
                    else:
                        tag_left_counter += 1
                        sleep(0.01)
                        if tag_left_counter > TAG_LEFT_MAX:
                            return 'NOTAG'

                elif pro_type == 'ISO14443A':
                    self.uartapi.write(ConstBitStream(hex='04022607').bytes)
                    sleep(0.03)
                    read_success = self.uartapi.read(2) # XX LEN
                    if ConstBitStream(bytes=read_success[0]) == ConstBitStream(hex='80'):
                        read_len = ConstBitStream(bytes=read_success[1]).int
                        if read_len > 0:
                            read_data = self.uartapi.read(read_len)
                            logging.debug('RAW ISO14443A {0} {1}'.format(read_len,ConstBitStream(bytes=read_success+read_data).hex))
                            read_colision = ConstBitStream(bytes=read_data[-1])
                            if not (read_colision[-2:].uint & 0b01 or read_colision[-2:].uint & 0b10):
                            #else CRC failed or collision, bitwise check if true of bit 1 (CRC fail) OR bit 0 (collision)
                                #second stage
                                self.uartapi.write(ConstBitStream(hex='0403932008').bytes)
                                sleep(0.03)
                                read_success = self.uartapi.read(2) # XX LEN
                                if ConstBitStream(bytes=read_success[0]) == ConstBitStream(hex='80'):
                                    read_len = ConstBitStream(bytes=read_success[1]).int
                                    if read_len > 3: #less than 3 is just empty inventory 0x8003 280000
                                        read_data = self.uartapi.read(read_len)
                                        logging.debug('RAW2 ISO14443A {0} {1}'.format(read_len,ConstBitStream(bytes=read_success+read_data).hex))
                                        rfid = ''
                                        for id_byte in read_data[3::-1]:
                                            rfid += id_byte
                                        if ConstBitStream(bytes=read_data[0]) == ConstBitStream(hex='88'): #longer uuid than 4 bytes
                                            self.uartapi.write(ConstBitStream(hex='04089370').bytes+read_data[:-3]+ConstBitStream(hex='28').bytes)
                                            sleep(0.03)
                                            self.uartapi.flush() #discart the useless answer
                                            self.uartapi.write(ConstBitStream(hex='0403952008').bytes)
                                            sleep(0.03)
                                            read_success = self.uartapi.read(2) # XX LEN
                                            if ConstBitStream(bytes=read_success[0]) == ConstBitStream(hex='80'):
                                                read_len = ConstBitStream(bytes=read_success[1]).int
                                                if read_len > 3: #less than 3 is just empty inventory 0x8003 280000
                                                    read_data = self.uartapi.read(read_len)
                                                    logging.debug('RAW3 ISO14443A {0} {1}'.format(read_len,ConstBitStream(bytes=read_success+read_data).hex))
                                                    rfid_part2 = ''
                                                    for id_byte in read_data[3::-1]:
                                                        rfid_part2 += id_byte
                                                    rfid = rfid_part2+rfid[:-1] #discart the last 0x88 that signified the 2 part uuid
                                                else:
                                                    #Not able to get the full UUID. Second part failed
                                                    return
                                            else:
                                                #Not able to get the full UUID. Second part failed
                                                return
                                        rfid = ConstBitStream(bytes=rfid).hex
                                        self.uartapi.flush()
                                        return rfid #Instant return if detection
                                else:
                                    logging.debug('RAW4 ISO14443A {0}'.format(ConstBitStream(bytes=read_success).hex))

                    elif ConstBitStream(bytes=read_success[0]) == ConstBitStream(hex='8E'):
                        logging.debug('Broken frame, something in the field')
                        tag_left_counter = 0

                    else:
                        sleep(0.01)
                        tag_left_counter += 1
                        if tag_left_counter > TAG_LEFT_MAX:
                            return 'NOTAG'

                else:
                    logging.error('Protocol {0} not supported'.format(pro_type))
                    return False
                self.uartapi.flush()
                self.uartapi_timeout_counter = 0 #successfully read

        except UartTimeoutError:
            self.uartapi_timeout_counter +=1
            if self.uartapi_timeout_counter > ECHO_TRIES:
                logging.error('Reader not responding, terminating')
                sys.exit()
            logging.error('Failed to communicate with the reader ')