"""Here is the infinite loop of the DB daemon that will work as a tcp service to all supported DBs or csvAPI
 related requests. It is not a class just a infinite script.
 Addresing of the store is done with backend ref not frontend IDs"""
__author__ = 'Georgios Lilis'
import os
import platform
import sys

PROJECT_DIR = os.path.abspath(__file__ + "/../../../")
sys.path.append(os.path.join(PROJECT_DIR, 'config'))
from conf_general import LOGGER_LVL, CURRENT_IP, SENSORS_API_TYPES, LOADS_API_TYPES
from conf_db import *
from middlewareutilities import get_middleware_details, get_middleware_objects
from math import sin, acos
import loggercolorizer
import zmq
import time
import copy
import signal
from db.csvapi import CsvApi, CsvApiError

# asdf asdf asdf
def validate_existance(currentmidid, currentid, currentmeasuretype):
    """A pretty complex validator, not need for API users to know it. Bulletproofs the dbAPI from mistakes in
        models. Very important.
        Not in any group of IDs"""
    global DB_MID_ID_SEN
    if currentmeasuretype not in SENSORS_API_TYPES and currentmeasuretype not in LOADS_API_TYPES:
        return False  # Not in any group of sensors types
    if currentmidid not in DB_MID_ID_SEN.keys():
        return False #unknown middleware
    found = False
    for id_measure in DB_MID_ID_SEN[currentmidid]:
        if id_measure[0] == currentid and id_measure[1] == currentmeasuretype:
            found = True
    return found

def register_requests(currentid, currentmeasuretype, requestqueue):
    """Holds the requests that are for the same file in order to parse them once only from csv.
    requestqueue is dict of dict. The outer keys are ref and the inner are the measuretype.
    """
    if currentid in requestqueue:
        if currentmeasuretype in requestqueue[currentid]:  # find if already registered
            return True  # Found
        else:  # Not registered
            requestqueue[currentid][currentmeasuretype] = True  # Just a boolean to the dict, we use only keys of dict
            return False  # Not found
    else:  # Not registered
        requestqueue[currentid] = dict([(currentmeasuretype, True)])
        return False  # Not found


def calc_power(apparentlist, powerfactorlist, measuretype):
    """Takes two lists of apparent power and power factor and returns the list of the requested measure type"""
    if len(apparentlist) != len(powerfactorlist):
        logger.error('First two input arguments in calc_power must have the same length.')
        return False
    if measuretype not in ['P', 'Q']:
        logger.error('This function supports only \'P\' and \'Q\' requests')
        return False
    toret = []
    for s, pf in zip(apparentlist, powerfactorlist):
        if measuretype == 'P':
            toret.append((float(s['Time']), float(s['Value']) * float(pf['Value'])))
        else:
            toret.append((float(s['Time']), float(s['Value']) * float(sin(acos(float(pf['Value']))))))
    return toret


def read_data(midlist, reflist, sentypelist, initendtime, resolution):
    """
    midlist: the list of incomming middleware the reflist equivalents belong
    reflist: the list of backend references used to find the correct storage
    sentypelist: the list of the sensor types to be able to discriminate between load and room sensor
    initendtime: a tuple(not list) that defines the start and end of the read
    resolution: the required resolution from DB. This ones is just mean type on the values returned in store
    forceres:  to be removed...
    """
    if STORAGE_TYPE is not 'CSV':
        logger.warning('This kind of storage is not supported, quitting')
        return False
    if not isinstance(reflist, list) or not isinstance(sentypelist, list) or not isinstance(midlist, list):
        logger.error('First three input arguments in db.read_data must be lists, nothing is read.')
        return False
    if len(reflist) != len(sentypelist) != len(midlist):
        logger.error('First three input arguments in db.read_data must have the same length, nothing is read.')
        return False
    (inittime, endtime) = initendtime
    if inittime == endtime and (inittime == -1 or endtime == -1):
        requestforlast = True
    else:
        requestforlast = False
    toreadfiletags = []
    for mid, ref, measuretype in zip(midlist, reflist, sentypelist):
        # Lets do validation tests to see if this sensor exist and if it is the correct measure type
        if validate_existance(mid, ref, measuretype):
            #Here for ASYNC and ZWAVE or for SYNC S, PF
            filetag = str(mid)+'_'+measuretype + '_' + str(ref)
            if filetag in toreadfiletags:
                continue  # Already registered for reading
            toreadfiletags.append(filetag)
        else:
            logger.error('Sensor REF: {0} with measure type: {1} from middleware: {2} not registered in store middleware. Nothing is read'.format(ref, measuretype, mid))
            return False
    data_read = dict()

    for filetag in toreadfiletags:
        try:
            starttime = time.time()
            if requestforlast:
                data_read[filetag] = csvapiinstance.csv_last_data_reader(filetag)
            else:
                data_read[filetag] = csvapiinstance.csv_block_data_reader(filetag, initendtime, resolution)
            if not data_read:
                raise RuntimeError('CSVapi error, no data returned')
            logger.debug('Fetched the data in:' + str(round((time.time() - starttime), 3)) + 'sec')
        except (RuntimeError, KeyError, CsvApiError, IOError) as e:
            logger.exception('Exception raised: ' + repr(e))
            return False


    data_to_return = []
    for mid, ref, measuretype in zip(midlist, reflist, sentypelist):
        #the filetag is the filename without the ".csv" and is the key to the returned dict
        filetag = str(mid)+'_'+measuretype + '_' + str(ref)
        tempsensordata = data_read[filetag]
        sensordata = []  # We have to create the tuple from the returned dict
        for sendata in tempsensordata:
            row = (float(sendata['Time']), float(sendata['Value']))
            sensordata.append(row)
        if requestforlast:  #To avoid sending lists of len 1
            data_to_return.append(sensordata[0])
        else:
            data_to_return.append(sensordata)
    return data_to_return


def write_data(midlist, reflist, sentypelist, timevaluelist):
    """
    midlist: the list of incomming middleware the reflist equivalents belong
    reflist: the list of backend references used to find the correct storage
    sentypelist: the list of the sensor types to be able to discriminate between load and room sensor
    timevaluelist: the list of tuples of (time, value)
    """
    if STORAGE_TYPE is not 'CSV':
        logger.warning('This kind of storage is not supported, quitting')
        return False
    if not isinstance(midlist, list) or not isinstance(reflist, list) or not isinstance(sentypelist, list) or not isinstance(timevaluelist, list):
        logger.error('All input arguments in db.write_data must be lists, nothing is written.')
        return False
    if len(midlist) != len(reflist) != len(sentypelist) != len(timevaluelist):
        logger.error('All input list in db.write_data must have the same length, nothing is written..')
        return False

    rowlisttosave = []
    for mid, ref, sentype, timevalue in zip(midlist, reflist, sentypelist, timevaluelist):
        if validate_existance(mid, ref, sentype):
            rowtosave = dict([(str(mid) + '_' + sentype + '_' + str(ref), {'Time': str(timevalue[0]), 'Value': str(timevalue[1])})])
            rowlisttosave.append(copy.deepcopy(rowtosave))
        else:
            logger.warning('Sensor REF: {0} with measure type: {1} from middleware: {2} not registered in store middleware. Skipping sample'.format(ref, sentype, mid))
            continue
    try:
        csvapiinstance.csv_data_writer(rowlisttosave)
        return True
    except CsvApiError:
        logger.error('Not written to csv file')
        return False


def exit_gracefully(_signo, _stack_frame):
    """Terminates the sockets gracefully without any left hanging open."""
    logger.info('Closing sockets and exiting gracefully')
    if DBsocket:
       DBsocket.setsockopt(zmq.LINGER, 0)
       DBsocket.close()
    if context:    
       context.destroy()
    #Internally the csvapi treats the atexit to save the pending messages to be written
    sys.exit(0)


if __name__ == "__main__":
    logger = loggercolorizer.getColorLogger(level=LOGGER_LVL, activate_email_logging=True, email_sender='DBdaemon@{0}'.format(CURRENT_IP))

    signal.signal(signal.SIGINT, exit_gracefully)
    signal.signal(signal.SIGTERM, exit_gracefully)
    if platform.system() == 'Linux':
        signal.signal(signal.SIGHUP, exit_gracefully)
        signal.signal(signal.SIGQUIT, exit_gracefully)

    """
    zmqmessage format:
    index 0: type READ or WRITE to DB
    index 1: sensorid, the backend equivalent, expect overlaps between interfaces, sensor type essential
    index 2: measurestype
    index 3: (inittime, endtime)
    index 4: resolution
    index 1-3 is a list to simplify the access to storage. BackendAPI take care of that
    BackendAPI takes care for most of the formating in zmqmessage. This daemon is just to interface the storage
    """
    middleware = get_middleware_details(midid=DB_MID_ID) #Execution will stop if it fails to find openBMS
    if middleware['middleware_type'] !='DB':
        logger.error('Could not find the DB middleware in openBMS, verify the ID in conf_db.py')
        sys.exit()
    ip = middleware['IP']
    rep_port = middleware['REP_port']
    pub_port = middleware['PUB_port'] #not used here

    #Given this ID, we get the connected objects to the middleware
    allobjects = get_middleware_objects(midid=DB_MID_ID)
    #The ids are the backend ones
    #The

    DB_MID_ID_SEN = dict() #A dictionary with keys the middleware ids and the value the list of tubles of (id, measure)
    for obj in allobjects: #create the external dict
        if not DB_MID_ID_SEN.has_key(obj['network_middleware']):
            DB_MID_ID_SEN[obj['network_middleware']] = list()
        DB_MID_ID_SEN[obj['network_middleware']].append((obj['ref'], obj['measure']))

    #----------------CSV SPECIFIC----------------#
    writelist = []
    for mid in DB_MID_ID_SEN.keys():
        for id_measure in DB_MID_ID_SEN[mid]:
            writelist.append('{0}_{1}_{2}.csv'.format(mid,id_measure[1],id_measure[0]))
    try:
        csvapiinstance = CsvApi(filelist=writelist, flushinterval = CSV_FLUSH_INTERVAL, nobuffer=False, truncate=LAST_ROWS_NUMBER)
    except CsvApiError:
        logger.error('DB daemon failed to start')
        sys.exit()
    #----------------CSV SPECIFIC----------------#

    # connect to port only after we have read the files,
    # otherwise the socket accumulates duplicate entries because it is not in a state to reply
    context = zmq.Context()
    DBsocket = context.socket(zmq.REP)
    logger.info('Starting the middleware "{0}" @ "{1}:{2}/{3}"'.format(middleware['name'], ip, rep_port, pub_port))
    DBsocket.bind('tcp://' + ip + ':' + rep_port)

    logger.info('DB daemon has started')

    while True:
        # Block and wait request for accessing the storage

        try:
            zmqmessage = DBsocket.recv_json()
            if len(zmqmessage) < 5:
                logger.warning("ZMQ message not complete or incorrect, skipping it")
                DBsocket.send_json(False)
                continue
            logger.debug(zmqmessage)
            readwrite = zmqmessage[0]
            middlewarelist = zmqmessage[1]
            sensorsidlist = zmqmessage[2]
            measurestypelist = zmqmessage[3]

            #Adaptation layer for functions that accept and return only lists
            if not isinstance(sensorsidlist, list) and not isinstance(measurestypelist, list) and not isinstance(middlewarelist, list):
                middlewarelist = [middlewarelist]
                sensorsidlist = [sensorsidlist]
                measurestypelist = [measurestypelist]
                inputislist = False
            else:
                inputislist = True

            if readwrite == "READ":
                initendtime = zmqmessage[4]
                resolution = zmqmessage[5]

                data = read_data(middlewarelist, sensorsidlist, measurestypelist, initendtime, resolution)
                if data:
                    if inputislist:
                        DBsocket.send_json(data)
                    else:
                        DBsocket.send_json(data[0])
                else:
                    DBsocket.send_json(False)
            elif readwrite == "WRITE":
                incommingdatalist = zmqmessage[4]
                result = write_data(middlewarelist, sensorsidlist, measurestypelist, incommingdatalist)
                DBsocket.send_json(result)  #This is always a boolean(no list is returned)
            else:
                raise Exception("Not recognized DB command {0}".format(readwrite))

        except Exception as e:
            logger.exception(repr(e))
            DBsocket.send_json(False)  #Register the failure with false without braking the loop/daemon