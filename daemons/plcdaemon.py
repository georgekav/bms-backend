"""Here is the infinite loop of the plc daemon that will work as a tcp service to all esmartAPI
 related requests. It is not a class just a infinite script"""
__author__ = 'Georgios Lilis'
import os
import platform
import sys

PROJECT_DIR = os.path.abspath(__file__ + "/../../../")
sys.path.append(os.path.join(PROJECT_DIR, 'config'))
from conf_general import PUB_MSG_TYPE, LOGGER_LVL, CURRENT_IP
from conf_plc import *
from middlewareutilities import get_middleware_details, get_middleware_refs
import uart
import loggercolorizer
import zmq
import plc.plcapi
import time
import cPickle
import signal
import copy
import threading


def save_buffer():
    bufferfile = open(BUFFERNAME, 'wb', buffering=0)
    pickler = cPickle.Pickler(bufferfile, cPickle.HIGHEST_PROTOCOL)
    pickler.dump(mainbuffer)
    bufferfile.close()


def createorread_buffer(forcepub=False):
    """Load or create the buffer. The outer dictionary is for the plcIDs. The inner dict keeps the status and measures
    comming from the plc network. The status is for the moment emulated."""
    global mainbuffer
    if forcepub:
        for plcid in mainbuffer.keys():
            mainbuffer[plcid]['ForcePUB'] = True
    else:
        bufferfile = None
        try:
            bufferfile = open(BUFFERNAME, 'rb', buffering=0)
            unpickler = cPickle.Unpickler(bufferfile)
            mainbuffer = unpickler.load()
            bufferfile.close()
            assert isinstance(mainbuffer, dict)
            for plcid in PLC_CHANNELID_ALL:
                if not plcid in mainbuffer.keys():
                    iddict = dict([('Status', dict()), ('Measures', dict()), ('RFID', None), ('ForcePUB', True)])
                    mainbuffer[plcid] = copy.deepcopy(iddict)
                    logger.info('Added plcid {0} to buffer'.format(plcid))

        except (EOFError, AssertionError):
            logger.info('Buffer file corrupted, creating a new')  # The creation is done in save_buffer()
            bufferfile.close()
            os.remove(BUFFERNAME)
            iddict = dict([('Status', dict()), ('Measures', dict()), ('RFID', None), ('ForcePUB', True)])
            mainbuffer = dict()
            for plcid in PLC_CHANNELID_ALL:
                mainbuffer[plcid] = copy.deepcopy(iddict)

        except IOError:
            logger.info('Buffer file doesn\'t exist, creating a new')  # The creation is done in save_buffer()
            iddict = dict([('Status', dict()), ('Measures', dict()), ('RFID', None), ('ForcePUB', True)])
            mainbuffer = dict()
            for plcid in PLC_CHANNELID_ALL:
                mainbuffer[plcid] = copy.deepcopy(iddict)

    return mainbuffer


def validate_plcid(plcid):
    if isinstance(plcid, str):
        plcid = int(plcid, 16)
    if plcid not in PLC_CHANNELID_ALL:
        logger.warning('Plcid {0} not found in defined list'.format(plcid))
        return False
    return plcid


def get_plc_status(message):
    """CACHED OPERATION
    The interpretation of the requested status is here
    We dont need api here. We will emulate the plug status reporting.
    Message is (ID, STATUS_TYPE)."""
    global mainbuffer
    plcid = validate_plcid(message[0])
    if not plcid:
        #Validation failed.
        return False

    status_type = message[1]
    logger.debug("Status request for: {0} by plugid {1}".format(status_type, plcid))

    if status_type == PLC_ACTUATOR_TYPES[0]:
        try:
            status = mainbuffer[plcid]['Status'][PLC_ACTUATOR_TYPES[0]]
            return status
        except KeyError:  # Default
            mainbuffer[plcid]['Status'][PLC_ACTUATOR_TYPES[0]] = DEFAULT_STAT
            status = mainbuffer[plcid]['Status'][PLC_ACTUATOR_TYPES[0]]
            return status
    elif status_type == PLC_ACTUATOR_TYPES[1]:
        try:
            status = mainbuffer[plcid]['Status'][PLC_ACTUATOR_TYPES[1]]
            return status
        except KeyError:
            mainbuffer[plcid]['Status'][PLC_ACTUATOR_TYPES[1]] = DEFAULT_STAT
            status = mainbuffer[plcid]['Status'][PLC_ACTUATOR_TYPES[1]]
            return status
    else:
        logger.warning('Unsupported type ' + str(status_type))
        return False


def get_powers(plcid, measuretype):
    """CACHED OPERATION
    This function will fetch the powers that are stored in the buffer instead of issuing a full plc request. It is
    responsibility of daemon to have them uptodate"""
    global mainbuffer
    plcid = validate_plcid(plcid)
    if not plcid:
        #Validation failed.
        return False
    else:
        try:
            toret = (mainbuffer[plcid]['Measures']['Time'], mainbuffer[plcid]['Measures'][measuretype])
            return toret
        except KeyError:
            logger.error('Not supported measure type: {0}, or not yet register an initial measure for {1}'.format(measuretype, plcid))
            return False


def get_connected_rfid(plcid):
    """CACHED OPERATION
    This function will fetch from the buffer the rfid of the connected load for the given plcid. It would return NONE is the
    actuator(plcid) doesnt have anything connected to it."""
    global mainbuffer
    plcid = validate_plcid(plcid)
    if not plcid:
        #Validation failed.
        return False
    try:
        return mainbuffer[plcid]['RFID']
    except KeyError:
        mainbuffer[plcid]['RFID'] = None
        return None


def execute_plc_cmd(message, api, interrupt=None):
    """The interpretation of the requested plc DIRECT cmd is here. By DIRECT we mean a real time command to be addressed to the
    embedded plc networks as soon as possible. This implies that the sync and async threads are blocks by the main zmq loop.
    If it is a action that need a auxiliary value, then the zmq message has
    length of 3. Message is (ID, COMMAND, [AUX]). Api is passed by the main loop."""
    global mainbuffer, plcPUBsocket

    cmd = message[1]
    if len(message) == 3:
        auxiliary = message[2]
    else:
        auxiliary = None

    if message[0] != 0xFFFF:  # 0xFFFF is the empty plcid async thread
        plcid = validate_plcid(message[0])
        if not plcid:
            return False
        logger.debug("Received PLC command {0} for plcid {1}".format(cmd, plcid))
    else:
        plcid = 0xFFFF

    if cmd in RELAY_CMD:
        newstatus = None
        if cmd == RELAY_CMD[0] and get_plc_status((plcid, PLC_ACTUATOR_TYPES[0])) == STATUS_OFF:
            res = api.set_relay_status(plcid=plcid, action=cmd)
            if res:
                newstatus = STATUS_ON
        elif cmd == RELAY_CMD[1] and get_plc_status((plcid, PLC_ACTUATOR_TYPES[0])) == STATUS_ON:
            res = api.set_relay_status(plcid=plcid, action=cmd)
            if res:
                newstatus = STATUS_OFF
        elif cmd == RELAY_CMD[2]:
            res = api.set_relay_status(plcid=plcid, action=cmd)
            if res:
                if get_plc_status((plcid, PLC_ACTUATOR_TYPES[0])) == STATUS_ON:
                    newstatus = STATUS_OFF
                else:
                    newstatus = STATUS_ON
        else:
            newstatus = 'OLD'
            res = True  # Nothing changed, but the request is true
        if newstatus in [STATUS_ON,STATUS_OFF]:
            reqtime = time.time()  # this time signifies when I "sense" there is a status change, not the time plc took...
            submsg = {'MID_ID': PLC_MID_ID,
                      'REF': plcid,
                      'MSG_TYPE': PUB_MSG_TYPE[0],
                      'PAYLOAD': {'Type': PLC_ACTUATOR_TYPES[0], 'Setting': newstatus, 'Time': reqtime}}
            plcPUBsocket.send_json(submsg)
            mainbuffer[plcid]['Status'][PLC_ACTUATOR_TYPES[0]] = newstatus
            logger.info('Relay {0} set at {1}'.format(plcid, cmd))
        elif newstatus == 'OLD':
            logger.info('Relay {0} already at {1}'.format(plcid, cmd))
        else: #newstatus = None
            logger.info('Relay {0} didnt send SACK'.format(plcid))
        return res

    elif cmd == DIMMER_CMD:
        if get_plc_status((plcid, PLC_ACTUATOR_TYPES[1])) != auxiliary:
            res = api.set_dimmer_status(plcid=plcid, dimsetting=auxiliary)
            if res:
                reqtime = time.time()  # this time signifies when I "sense" there is a status change, not the time plc took...
                submsg = {'MID_ID': PLC_MID_ID,
                      'REF': plcid,
                      'MSG_TYPE': PUB_MSG_TYPE[0],
                      'PAYLOAD': {'Type': PLC_ACTUATOR_TYPES[1], 'Setting': auxiliary, 'Time': reqtime}}
                plcPUBsocket.send_json(submsg)
                mainbuffer[plcid]['Status'][PLC_ACTUATOR_TYPES[1]] = auxiliary
                logger.info('Dimmer {0} set at {1}'.format(plcid, auxiliary))
        else:
            res = True  # Nothing changed, but the request is true
        return res

    elif cmd == 'SYNCPLC':
        powers = api.req_sync_powers(plcch=plcid)
        if powers:
            if not 'S' in powers:
                logger.warning('Incomming packet is not a power measure type')
                return False
            if powers['S'] > 5000:
                logger.warning('Plcid {0} stuck, RESET it'.format(plcid))
                return False
            changed = False  # Signifies that there is a change for the given plcid for at least one of the keys. Send one PUB
            forcepub = mainbuffer[plcid]['ForcePUB']
            for key in powers.keys():
                try:
                    #only in difference greater than PLC_POWER_RESOLUTION publish
                    if (forcepub or abs(mainbuffer[plcid]['Measures'][key] - powers[key]) > PLC_POWER_RESOLUTION) and key != 'Time' and not changed:
                        submsg = {'MID_ID': PLC_MID_ID,
                                  'REF': plcid,
                                  'MSG_TYPE': PUB_MSG_TYPE[1],
                                  'PAYLOAD': powers}
                        plcPUBsocket.send_json(submsg)
                        changed = True
                        mainbuffer[plcid]['ForcePUB'] = False
                        break  # Nothing else to do, we sent already the PUB of all powers
                except KeyError:
                    changed = True
                    continue  # Check the rest keys since this quantity doesnt exist in buffer, we will update the buffer in next command anyway
            mainbuffer[plcid]['Measures'] = powers  # keep the buffer up to date at all time

            name = 'Plug: {0}'.format(plcid)
            value = name, 'S:', powers['S']
            infostr = '{0} {1} {2}'.format(*value)
            if changed:  # Output shows change in value with the *
                logger.info(infostr + ' * ')
            else:
                logger.info(infostr)
            if auxiliary:
                return get_powers(plcid, auxiliary)  #this is for the nobuffer operation of backendapi, you return this format of data
            else:
                return powers
        else:
            return False

    elif cmd == 'ASYNCPLC':
        #asyncmsg list is 0: the plcid, 1: the module powers, 2: the module state.
        asyncmsg = api.wait_asynq_frame(interrupt)
        if asyncmsg:
            currenttime = time.time()  # We register as soon as possible at arrival
            msg_type, plcid, payload = asyncmsg
            if msg_type == 'Measurement':
                if plcid not in P_CAPABLE_PLUGS:
                    logger.warning('Incomming packet is not from a recognized async plcid, skipping it, {0}'.format(plcid))
                    return False
                powers = dict()
                try:
                    power = payload[1][1]
                    state = payload[0]
                    if power > 0x7FFF: #more that means that it negative aka wrong
                        power = 0 #Temporary workaround...
                except IndexError:
                    logger.warning('Empty payload by module {0}'.format(plcid))
                    return False

                powers['P'] = power  #only active power now
                powers['Time'] = currenttime

                changed = False  # Signifies that there is a change for the given plcid for at least one of the keys. Send one PUcurrenttimrs dict, dirty way, but precise
                forcepub = mainbuffer[plcid]['ForcePUB'] #We store it to a variable because in for loop will change
                for key in powers.keys():
                    try:
                        #only in difference greater than PLC_POWER_RESOLUTION publish
                        if (forcepub or abs(mainbuffer[plcid]['Measures'][key] - powers[key]) > PLC_POWER_RESOLUTION) and key != 'Time' and not changed:
                            submsg = {'MID_ID': PLC_MID_ID,
                                      'REF': plcid,
                                      'MSG_TYPE': PUB_MSG_TYPE[1],
                                      'PAYLOAD': powers}
                            plcPUBsocket.send_json(submsg)
                            changed = True
                            mainbuffer[plcid]['ForcePUB'] = False
                            break  # Nothing else to do, we sent already the PUB of all powers
                    except KeyError:
                        changed = True
                        continue  # Check the rest keys since this quantity doesnt exist in buffer, we will update the buffer in next command anyway

                #For the state report, 0: is the type, 1: is the value
                if forcepub or get_plc_status((plcid, state[0])) != state[1]:
                    submsg = {'MID_ID': PLC_MID_ID,
                          'REF': plcid,
                          'MSG_TYPE': PUB_MSG_TYPE[0],  #Status type
                          'PAYLOAD': {'Type': state[0], 'Setting': state[1], 'Time': currenttime}}
                    mainbuffer[plcid]['ForcePUB'] = False
                    changed = True
                    plcPUBsocket.send_json(submsg)

                #keep the buffer up to date at all time
                mainbuffer[plcid]['Measures'] = powers
                mainbuffer[plcid]['Status'][state[0]] = state[1]

                logstr = 'Plug: {0} {1}:{2} {3}: {4}'.format(plcid, PUB_MSG_TYPE[1], powers, PUB_MSG_TYPE[0], state)
                if changed:  # Output shows change in value with the *
                    logstr += ' * '
                logger.info(logstr)
                return True
            elif msg_type == 'Rfid':
                try:
                    changed = False
                    if payload[0][1]:
                        load_uid = payload[0][1]
                    else:
                        load_uid = None
                    if mainbuffer[plcid]['RFID'] != load_uid:  # In case plug sends the same, we have to filter just in case.(this will be moved in final plcAPI)
                        mainbuffer[plcid]['RFID'] = load_uid
                        submsg = {'MID_ID': PLC_MID_ID,
                                  'REF': plcid,
                                  'MSG_TYPE': PUB_MSG_TYPE[2],
                                  'PAYLOAD': {'LD_UID': load_uid, 'Time': currenttime}}
                        plcPUBsocket.send_json(submsg)
                        changed = True
                    logstr = 'Plug: {0} {1}: {2}'.format(plcid, PUB_MSG_TYPE[2],load_uid)
                    if changed:  # Output shows change in value with the *
                        logstr += ' * '
                    logger.info(logstr)
                    return True
                except KeyError:  # Should never ever arrive here since 'RFID' key is given at the creation of dict()
                    mainbuffer[plcid]['RFID'] = load_uid
                    return True
                except IndexError: #problem with the rfid packet
                    logger.error('Problem with the RFID packet: ' + str(asyncmsg))
                    return False
        else:
            return False
    else:
        logger.warning('Command not defined for PLC cmd: ' + cmd)
        return False


def execute_daemon_cmd(message):
    """The interpretation of the requested DAEMON only cmd is here. This is a command that would not block the embedded networks
    for serving it.
    If it is a cmd that need a auxiliary value, then the zmq message has
    length of 3. Message is (ID, COMMAND, [AUX])."""
    global mainbuffer, plcPUBsocket
    plcid = validate_plcid(message[0])
    if not plcid:
        #Validation failed.
        return False
    cmd = message[1]
    if len(message) == 3:
        auxiliary = message[2]
    else:
        auxiliary = None
    logger.debug("Received Daemon command {0} for plcid {1}".format(cmd, plcid))

    if cmd in PLC_ACTUATOR_TYPES:
        return get_plc_status(message)  # This accepts the full message
    elif cmd == 'POWER':
        return get_powers(plcid, auxiliary)
    elif cmd == 'RFID':
        return get_connected_rfid(plcid)
    elif cmd == 'RTTEST':
        """Temporary for RT testing until rfid installed in plug. Sending to PUB the PUB equivalent of the REQ incomming packet."""
        auxiliary = str(auxiliary) #it is in format 0xFFFFFFFF without :/-
        if auxiliary=='None':
            load_uid=None
        else:
            load_uid = ''
            for i in range(0, len(auxiliary), 2):
                load_uid += auxiliary[i:i+2]
                load_uid += ':'
            load_uid = load_uid[:-1] #remove last :
        try:
            logger.info((PUB_MSG_TYPE[2], plcid, load_uid))
            if mainbuffer[plcid]['RFID'] != load_uid:  # In case plug sends the same, we have to filter just in case.(this will be moved in final plcAPI)
                mainbuffer[plcid]['RFID'] = load_uid
                submsg = {'MID_ID': PLC_MID_ID,
                          'REF': plcid,
                          'MSG_TYPE': PUB_MSG_TYPE[2],
                          'PAYLOAD': {'LD_UID': load_uid}}
                plcPUBsocket.send_json(submsg)
                return True
            else:
                return False
        except KeyError:  # Should never ever arrive here since 'RFID' key is given at the creation of dict()
            mainbuffer[plcid]['RFID'] = load_uid
            return True
    else:
        #Should never arrive here, if this is the case then main loop did a mistake and forward the message to
        #execute_daemon_cmd function
        logger.warning('Command not defined for Daemon cmd {0}'.format(cmd))
        return False


#---------------------------------------------Threads Definitions------------------------------------------------------#
def async_power_thread(esmartapi, asyncevent):
    """This is for the async power collect thread that should always listen for packets."""
    global mainbuffer
    begintime = time.time()
    while True:
        asyncevent.wait()
        if time.time() - begintime > 3600:
            #Force resend of PUB message once in a while in case datacollector or websocket missed it
            begintime = time.time()
            createorread_buffer(forcepub=True)
        res = execute_plc_cmd((0xFFFF, 'ASYNCPLC'), esmartapi, asyncevent)
        if res:
            save_buffer()


def sync_power_thread(esmartapi, syncevent, asyncevent):
    """This is for the sync power collect thread that run every once in a while. It has priority against async."
    Async blocks for all the loop"""
    global mainbuffer, mainevent
    while True:
        timeout = time.time() + SYNC_SAMPLE_RATE
        for plcid in S_PF_CAPABLE_PLUGS:
            syncevent.wait()
            asyncevent.clear()
            mainevent.clear()
            try:
                res = execute_plc_cmd((plcid, 'SYNCPLC'), esmartapi)
                if res:  # update our buffer store
                    save_buffer()
            finally:
                asyncevent.set()
                mainevent.set()

        remainingtime = timeout - time.time()
        while remainingtime > 0:
            time.sleep(remainingtime)
#----------------------------------END Threads Definitions-------------------------------------------------------------#


def exit_gracefully(_signo, _stack_frame):
    """Terminates the sockets gracefully without any left hanging open."""
    logger.info('Closing sockets and exiting gracefully')
    if plcREPsocket:
       plcREPsocket.setsockopt(zmq.LINGER, 0)
       plcREPsocket.close()
    if plcPUBsocket:
       plcPUBsocket.setsockopt(zmq.LINGER, 0)
       plcPUBsocket.close()
    if context:
       context.destroy()
    save_buffer()
    sys.exit(0)


if __name__ == "__main__":

    logger = loggercolorizer.getColorLogger(level=LOGGER_LVL, activate_email_logging=True, email_sender='PLCdaemon@{0}'.format(CURRENT_IP))

    signal.signal(signal.SIGINT, exit_gracefully)
    signal.signal(signal.SIGTERM, exit_gracefully)
    if platform.system() == 'Linux':
        signal.signal(signal.SIGHUP, exit_gracefully)
        signal.signal(signal.SIGQUIT, exit_gracefully)

    middleware = get_middleware_details(midid=PLC_MID_ID) #Execution will stop if it fails to find openBMS
    if middleware['middleware_type'] !='PLC':
        logger.error('Could not find the PLC middleware in openBMS, verify the ID in conf_plc.py')
        sys.exit()
    ip = middleware['IP']
    rep_port = middleware['REP_port']
    pub_port = middleware['PUB_port']
    logger.info('Starting the middleware "{0}" @ "{1}:{2}/{3}"'.format(middleware['name'], ip, rep_port,pub_port))
    context = zmq.Context()
    plcREPsocket = context.socket(zmq.REP)
    plcREPsocket.bind('tcp://' + ip + ':' + rep_port)
    plcPUBsocket = context.socket(zmq.PUB)
    plcPUBsocket.bind('tcp://' + ip + ':' + pub_port)

    S_PF_CAPABLE_PLUGS = ()  #This will be empty, no way to determine from the API the type of backend
    PLC_CHANNELID_ALL = P_CAPABLE_PLUGS = get_middleware_refs(midid=PLC_MID_ID) #get_middleware_refs is a list so we extend

    mainbuffer = createorread_buffer()
    serialapi = uart.SerialInterface(usbconf=PLC_USB_PARAM)
    ret = serialapi.connect()
    if ret:
        esmartapi = plc.plcapi.EsmartApi(uartapi=serialapi)
    else:
        logger.error("Device error, exiting")
        sys.exit()

    #Check for master
    if not esmartapi.check_master_plug():
        logger.error("Master plug cannot be reached, please validate the connection")
        sys.exit()

    #Find connected modules
    modulesdiscovered = esmartapi.req_subnet_nodes(maxaddress=NETWORK_DISCOVERY_MAX_ADDRESS)
    iddiscovered = list()
    for nd in modulesdiscovered:
        ch1 = nd+1
        ch2 = nd+2
        iddiscovered.extend([ch1, ch2])
        if ch1 not in PLC_CHANNELID_ALL:
            logger.warning('ID {0} not in openBMS list'.format(ch1))
        if ch2 not in PLC_CHANNELID_ALL:
            logger.warning('ID {0} not in openBMS list'.format(ch2))
    for plcid in PLC_CHANNELID_ALL:
        if plcid not in iddiscovered:
            logger.warning('OpenBMS Ref {0} not found in the plc network'.format(plcid))

    # ---------Multiple threads preparation---------#
    asyncevent = threading.Event()
    syncevent = threading.Event()
    mainevent = threading.Event()  # This signals that event can proceed, sync power finnished current work.
    asyncthread = threading.Thread(target=async_power_thread, args=(esmartapi, asyncevent))
    syncthread = threading.Thread(target=sync_power_thread, args=(esmartapi, syncevent, asyncevent))
    syncthread.start()
    asyncthread.start()
    #Initializing all threads
    asyncevent.set()
    syncevent.set()
    mainevent.set()
    asyncstate = True

    #Specify the routing criterions
    daemon_commands = list(PLC_ACTUATOR_TYPES) + ['POWER'] + ['RFID'] + ['RTTEST']
    plc_commands = list(RELAY_CMD) + [DIMMER_CMD] + ['SYNCPLC'] + ['ASYNCPLC']

    logger.info('PLC daemon has started')
    while True:
        try:
            zmqmessages = plcREPsocket.recv_json()
            #normalising to list. Since Json doesnt support tuple but even a single zmqmessage(the tuple) will be a list,
            #we are making sure the first element to be list)
            reqislist = True
            if not isinstance(zmqmessages[0], list):
                zmqmessages = [zmqmessages]
                reqislist = False
            res = []
            for zmqmessage in zmqmessages:
                zmqmessage[1] = zmqmessage[1].upper()
                if zmqmessage[1] in daemon_commands:
                    res0 = execute_daemon_cmd(zmqmessage)
                    res.append(res0)
                elif zmqmessage[1] in plc_commands:
                    # Only in plc we have potential buffer update so only here we update explicitely the !file!
                    try:
                        syncevent.clear()
                        mainevent.wait()  # wait permition to start
                        asyncstate = asyncevent.isSet()
                        asyncevent.clear()
                        res0 = execute_plc_cmd(zmqmessage, esmartapi)
                        res.append(res0)
                    finally:
                        if asyncstate:  # Free again the async thread
                            asyncevent.set()
                        #Let the async potentialy catch any incomming power, because the action was for async!!!
                        #Workaround that may slow down the system if it is full of async plugs
                        if zmqmessage[0] in P_CAPABLE_PLUGS:
                            time.sleep(1)
                        syncevent.set()
                    if res0:  # If there is a change update our buffer store
                        #CAUTION this writes constanlty to IO device.
                        #Remove it in production
                        save_buffer()
                else:
                    logger.warning('Unknown command received: {0}'.format(zmqmessage[1]))
                    res.append(False)

            if reqislist:
                plcREPsocket.send_json(res)
            else:
                plcREPsocket.send_json(res[0])
        except Exception as e:
            logger.exception(repr(e))
            plcREPsocket.send_json(False)  #Register the failure with false without braking the loop/daemon