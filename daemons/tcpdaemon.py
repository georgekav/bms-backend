import os
import sys
import platform
import zmq
import signal
from conf_tcp import TCP_REMOTE_PORT, CURRENT_IP


def exit_gracefully(_signo, _stack_frame):
    """Terminates the sockets gracefully without any left hanging open."""
    if zmqsocket:
       zmqsocket.setsockopt(zmq.LINGER, 0)
       zmqsocket.close()
    if context:    
       context.destroy()
    sys.exit(0)


context = zmq.Context()
zmqsocket = context.socket(zmq.REP)
zmqsocket.bind('tcp://' + CURRENT_IP + ':'+TCP_REMOTE_PORT)
print "Starting sleep daemon"

signal.signal(signal.SIGINT, exit_gracefully)
signal.signal(signal.SIGTERM, exit_gracefully)
if platform.system() == 'Linux':
    signal.signal(signal.SIGHUP, exit_gracefully)
    signal.signal(signal.SIGQUIT, exit_gracefully)

while True:
    try:
        message = zmqsocket.recv_json()
        zmqsocket.send_json(True)
        if message == 'SLEEP':
            print 'Got sleep msg'
            os.system(r'%windir%\system32\rundll32.exe powrprof.dll,SetSuspendState Sleep')
    except Exception as e:
        zmqsocket.send_json(False)