"""Here is the infinite loop of the rfid daemon that will work as a tcp service to all zwaveAPI related requests"""
__author__ = 'Georgios Lilis'
import os
import platform
import sys

PROJECT_DIR = os.path.abspath(__file__ + "/../../../")
sys.path.append(os.path.join(PROJECT_DIR, 'config'))
from conf_general import  LOGGER_LVL, CURRENT_IP, PUB_MSG_TYPE
from conf_rfid import RFID_USB_PARAM_INIT, RFID_USB_PARAM_HS, TIME_TO_HIBERNATE, SCANNED_PROTOCOLS, USERREC_MID_ID, NO_TAG_STRING
from middlewareutilities import get_middleware_details
import zmq
import signal
import uart
import loggercolorizer
from rfid import rfidapi
import time


def exit_gracefully(_signo, _stack_frame):
    """Terminates the sockets gracefully without any left hanging open."""
    if rfidPUBsocket:
       rfidPUBsocket.setsockopt(zmq.LINGER, 0)
       rfidPUBsocket.close()
    if context:
       context.destroy()
    sys.exit(0)

if __name__ == "__main__":
    logger = loggercolorizer.getColorLogger(level=LOGGER_LVL, activate_email_logging=True, email_sender='RFIDdaemon@{0}'.format(CURRENT_IP))
    signal.signal(signal.SIGINT, exit_gracefully)
    signal.signal(signal.SIGTERM, exit_gracefully)
    if platform.system() == 'Linux':
        signal.signal(signal.SIGHUP, exit_gracefully)
        signal.signal(signal.SIGQUIT, exit_gracefully)


    middleware = get_middleware_details(midid=USERREC_MID_ID) #Execution will stop if it fails to find openBMS
    if middleware['middleware_type'] !='IP':
        logger.error('Could not find the User Recognition middleware in openBMS, verify the ID in conf_rfid.py')
        sys.exit()
    ip = middleware['IP']
    rep_port = middleware['REP_port'] #not used here
    pub_port = middleware['PUB_port']
    context = zmq.Context()
    rfidPUBsocket = context.socket(zmq.PUB)
    rfidPUBsocket.bind('tcp://' + ip + ':' + pub_port)
    logger.info('Starting the middleware "{0}" @ "{1}:{2}/{3}"'.format(middleware['name'], ip, rep_port, pub_port))

    serialapi = uart.SerialInterface(usbconf=RFID_USB_PARAM_INIT)
    ret = serialapi.connect()
    if ret:
        rfidtagapi = rfidapi.RfidTagApi(uartapi=serialapi)
    else:
        logger.error("Device error, exiting")
        sys.exit()

    logger.info('RFID daemon has started')
    logger.info('Verifing the connection')
    if rfidtagapi.echo_check():
        logger.info('RFID reader started')
    else:
        logger.error('Unable to communicate with reader')
        sys.exit()

    rfidtagapi.set_baud(RFID_USB_PARAM_HS.BaudRate)
    logger.info('Changing baud rate')
    last_rfid = ''
    supported_pro = SCANNED_PROTOCOLS

    while True:
        try:
            index = 0
            while index < len(supported_pro):
                rfid = rfidtagapi.read_tag(pro_type=supported_pro[index])
                if rfid and rfid != last_rfid:
                    if rfid == NO_TAG_STRING:
                        last_rfid = rfid
                        logger.info('No tag in the field')
                    else:
                        last_rfid = rfid
                        logger.info("Tag id: {0}".format(rfid))
                    msg = {'MID_ID': USERREC_MID_ID,
                              'MSG_TYPE': PUB_MSG_TYPE[4],
                              'PAYLOAD': {'User_uuid': rfid, 'Time': time.time()}}
                    rfidPUBsocket.send_json(msg)
                if rfid == NO_TAG_STRING: #Change to next protocol
                    index +=1
                rfidtagapi.hibernate(TIME_TO_HIBERNATE)
        except Exception as e:
            logger.exception(repr(e))
