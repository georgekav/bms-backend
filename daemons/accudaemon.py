__author__ = 'Georgios Lilis'
import os
import platform
import sys

PROJECT_DIR = os.path.abspath(__file__ + "/../../../")
sys.path.append(os.path.join(PROJECT_DIR, 'config'))
from conf_general import LOGGER_LVL, PUB_MSG_TYPE, SENSORS_API_TYPES, CURRENT_IP
from conf_openbms import OPENBMS_IP
from conf_accumulator import PLC_MID_ID, ZWAVE_MID_ID, DB_MID_ID, DB_POLL_TIMEOUT
from middlewareutilities import get_middleware_refs, get_middleware_details
import loggercolorizer
import zmq
import signal

def report_to_DBdaemon(midlist, reflist, sentypelist, timevaluelist):
    global DBsocketlist, DBpollerlist, DBmessageslist, DBipportlist
    logmsg = 'MID:{0} ID:{1} TYPE:{2} SAMPLE:{3}'.format(midlist, reflist, sentypelist, timevaluelist)
    logger.info(logmsg)
    for idx, val in enumerate(DBsocketlist):
        #DBsocket and DBpoller have the same length
        DBsocket = DBsocketlist[idx]
        DBpoller = DBpollerlist[idx]
        DBmessages = DBmessageslist[idx]
        (ip,port) = DBipportlist[idx]
        if DBmessages:
            #If we have pending we add to them the new
            (midlist_old, reflist_old, sentypelist_old, timevaluelist_old) = DBmessages
            midlist_tosend = midlist_old + midlist
            reflist_tosend = reflist_old + reflist
            sentypelist_tosend = sentypelist_old + sentypelist
            timevaluelist_tosend = timevaluelist_old + timevaluelist
        else:
            midlist_tosend = midlist
            reflist_tosend = reflist
            sentypelist_tosend = sentypelist
            timevaluelist_tosend = timevaluelist
        DBsocket.send_json(('WRITE',midlist_tosend, reflist_tosend, sentypelist_tosend, timevaluelist_tosend))
        socks = dict(DBpoller.poll(timeout=DB_POLL_TIMEOUT))
        if socks.get(DBsocket) == zmq.POLLIN:
            ret = DBsocket.recv_json()
        else:
            ret = None
        if not ret:
            logger.warning('DB write to {0} failed, {1} packets still waiting re-transmition'.format(ip, len(reflist_tosend)))
            DBsocket.close()
            DBpoller.unregister(DBsocket)
            DBsocket = context.socket(zmq.REQ)
            DBsocket.connect('tcp://' + ip + ':' + port)
            DBsocket.setsockopt(zmq.LINGER, 0)
            DBpoller.register(DBsocket, zmq.POLLIN)
            DBsocketlist[idx] = DBsocket
            DBpollerlist[idx] = DBpoller
            #Save the list
            DBmessageslist[idx] = (midlist_tosend, reflist_tosend, sentypelist_tosend, timevaluelist_tosend)
        else:
            logger.debug('DB write to {0} was successful'.format(ip))
            #Erase the old messages since it was successful
            DBmessageslist[idx] = ()

    return True

def exit_gracefully(_signo, _stack_frame):
    """Terminates the sockets gracefully without any left hanging open."""
    logger.info('Closing sockets and exiting gracefully')
    if SUBsocket:
       SUBsocket.setsockopt(zmq.LINGER, 0)
       SUBsocket.close()
    for socket in DBsocketlist:
       socket.setsockopt(zmq.LINGER, 0)
       socket.close()
    if context:    
       context.destroy()
    sys.exit(0)


if __name__ == "__main__":
    logger = loggercolorizer.getColorLogger(level=LOGGER_LVL, activate_email_logging=True, email_sender='ACCUdaemon@{0}'.format(CURRENT_IP))

    signal.signal(signal.SIGINT, exit_gracefully)
    signal.signal(signal.SIGTERM, exit_gracefully)
    if platform.system() == 'Linux':
        signal.signal(signal.SIGHUP, exit_gracefully)
        signal.signal(signal.SIGQUIT, exit_gracefully)

    #Given these PLC_MID_ID and ZWAVE_MID_ID, we get the connected objects to the middleware
    ZWAVE_NODEID_LIST = list()
    PLC_CHANNELID_LIST = list() #We have a common for this for now
    for plcmid in PLC_MID_ID:
        PLC_CHANNELID_LIST.extend(get_middleware_refs(midid=plcmid)) #get_middleware_refs is a list so we extend
    for zwmid in ZWAVE_MID_ID:
        ZWAVE_NODEID_LIST.extend(get_middleware_refs(midid=zwmid))

    context = zmq.Context()
    DBsocketlist = list()
    DBpollerlist = list()
    DBmessageslist = list()
    DBipportlist = list()
    for mid_id in DB_MID_ID:
        DBsocket = context.socket(zmq.REQ) #it should be inside the for
        store_middleware = get_middleware_details(midid=mid_id)
        if store_middleware['middleware_type'] != 'DB':
            logger.error('Could not find the DB middleware {0} in openBMS, verify the ID in conf_accu.py'.format(mid_id))
        else:
            ip = store_middleware['IP']
            port = store_middleware['REP_port']
            if ip == '127.0.0.1':
                logger.info('The middleware address is configured as 127.0.0.1. Binding the socket to the default IP of openBMS {0}'.format(OPENBMS_IP))
                ip = OPENBMS_IP
                #OPENBMS_IP not current so that if it is
                #not really with the OPENBMS on the same IP to through error of unable to get that IP
            logger.info('Collecting to STORE middleware "{0}" @ "{1}:{2}"'.format(store_middleware['name'], ip, port))
            DBsocket.connect('tcp://' + ip + ':' + port)
            DBsocket.setsockopt(zmq.LINGER, 0)
            DBpoller = zmq.Poller()
            DBpoller.register(DBsocket, zmq.POLLIN)
            DBipportlist.append((ip,port))
            DBsocketlist.append(DBsocket)
            DBpollerlist.append(DBpoller)
            DBmessageslist.append(()) #empty tuple

    if len(DBsocketlist) == 0:
        logger.error('Could not find any DB middleware in openBMS, verify the ID in conf_accu.py')
        sys.exit()

    SUBsocket = context.socket(zmq.SUB)
    for mid_id in PLC_MID_ID+ZWAVE_MID_ID:
        middleware = get_middleware_details(midid=mid_id)
        if middleware['middleware_type'] not in ['PLC', 'ZWAVE']:
            logger.error('Could not find the PLC or ZWAVE middleware in openBMS, verify the ID in conf_accu.py')
            sys.exit()
        ip = middleware['IP']
        port = middleware['PUB_port']
        if ip == '127.0.0.1':
            logger.info('The middleware address is configured as 127.0.0.1. Binding the socket to the default IP of openBMS {0}'.format(OPENBMS_IP))
            ip = OPENBMS_IP
            #OPENBMS_IP not current so that if it is
            #not really with the OPENBMS on the same IP to through error of unable to get that IP

        logger.info('Collecting from middleware "{0}" @ "{1}:{2}"'.format(middleware['name'], ip, port))
        SUBsocket.connect('tcp://' + ip + ':' + port)
    SUBsocket.setsockopt(zmq.SUBSCRIBE, '')
    #------End of ZMQ------#

    rowlisttosave = []

    logger.info('Remote data collector (accu) have been started')
    midlist = []
    reflist = []
    sentypelist = []
    timevaluelist = []

    while True:
        receivedmsg = SUBsocket.recv_json()
        if receivedmsg['MSG_TYPE'] != PUB_MSG_TYPE[1]:
            continue  # We need only the MEASURE type
        try:
            ref = receivedmsg['REF']  # It is in str
            if receivedmsg['MID_ID'] in PLC_MID_ID:
                #PLC specific
                if ref not in PLC_CHANNELID_LIST:
                    logger.warning('Sensor REF: {0} from middleware: {1} not registered in network middleware. Skipping sample'.format(ref,  receivedmsg['MID_ID']))
                    continue
                for sen in receivedmsg['PAYLOAD'].keys():
                    if sen == 'Time': continue #Ignore key 'Time'
                    midlist.append(receivedmsg['MID_ID'])
                    reflist.append(ref)
                    sentypelist.append(sen)
                    timevaluelist.append((receivedmsg['PAYLOAD']['Time'],receivedmsg['PAYLOAD'][sen]))

            elif receivedmsg['MID_ID'] in ZWAVE_MID_ID:
                #ZWAVE specific
                if ref not in ZWAVE_NODEID_LIST:
                    logger.warning('Sensor REF: {0} from middleware: {1} not registered in network middleware. Skipping sample'.format(ref,  receivedmsg['MID_ID']))
                    continue
                sensorname = receivedmsg['PAYLOAD']['Quantity']
                if sensorname not in SENSORS_API_TYPES:
                    logger.warning('Received PUB message that dont correspond to zwave sensor capabilities privided by zwave daaemon')
                    continue
                sensorvalue = receivedmsg['PAYLOAD']['Value']
                timeofvalue = receivedmsg['PAYLOAD']['Time']
                midlist.append(receivedmsg['MID_ID'])
                reflist.append(ref)
                sentypelist.append(sensorname)
                timevaluelist.append((timeofvalue, sensorvalue))
            else:
                logger.warning('Received MID_ID: {0} which collector is not setup to collect.'.format(receivedmsg['MID_ID']))
                continue

            res = report_to_DBdaemon(midlist, reflist, sentypelist, timevaluelist)
            if res:
                #We reset the lists
                midlist = []
                reflist = []
                sentypelist = []
                timevaluelist = []

        except (KeyError, IndexError, ValueError) as e:
            logger.exception('Possible corrupted zmq PUB packet: ' + repr(e))
            continue