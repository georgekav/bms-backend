"""Here is the infinite loop of the zwave daemon that will work as a tcp service to all zwaveAPI related requests"""
__author__ = 'Georgios Lilis'
import os
import platform
import sys

PROJECT_DIR = os.path.abspath(__file__ + "/../../../")
sys.path.append(os.path.join(PROJECT_DIR, 'config'))
from conf_general import  SENSORS_API_TYPES, PUB_MSG_TYPE, LOGGER_LVL, CURRENT_IP
from conf_zwave import *
from middlewareutilities import get_middleware_details, get_middleware_refs
import zmq
import time
import cPickle
import signal
import copy
import threading
import os
import uart
from zwave.zwaveapi import AeotecApi
import loggercolorizer

def save_buffer():
    bufferfile = open(BUFFERNAME, 'wb', buffering=0)
    pickler = cPickle.Pickler(bufferfile, cPickle.HIGHEST_PROTOCOL)
    pickler.dump(mainbuffer)
    bufferfile.close()


def createorread_buffer(forcepub=False):
    """Load or create the buffer. The outer dictionary is for the zwaveIDs. The inner is just measures for the moment.
    In the future we can support more if we want(like status report). The sensorID is an int."""
    global mainbuffer
    if forcepub:
        for senid in mainbuffer.keys():
            mainbuffer[senid]['ForcePUB'] = True
    else:
        try:
            bufferfile = None
            bufferfile = open(BUFFERNAME, 'rb', buffering=0)
            unpickler = cPickle.Unpickler(bufferfile)
            mainbuffer = unpickler.load()
            bufferfile.close()
            assert isinstance(mainbuffer, dict)
            for senid in ZWAVE_NODEID_LIST:
                if not senid in mainbuffer.keys():
                    iddict = dict([('Measures', dict()), ('ForcePUB', True)])
                    mainbuffer[senid] = copy.deepcopy(iddict)
                    logger.info('Added senid ' + str(senid) + ' to buffer')

        except (EOFError, AssertionError):
            logger.info('Buffer file corrupted, creating a new')  # The creation is done in save_buffer()
            bufferfile.close()
            os.remove(BUFFERNAME)
            sensordict = dict()
            for sen in SENSORS_API_TYPES:
                sensordict[sen] = None
            iddict = dict([('Measures', dict()), ('ForcePUB', True)])
            mainbuffer = dict()
            for senid in ZWAVE_NODEID_LIST:
                mainbuffer[senid] = copy.deepcopy(iddict)

        except IOError:
            logger.info('Buffer file doesn\'t exist, creating a new')  # The creation is done in save_buffer()
            sensordict = dict()
            for sen in SENSORS_API_TYPES:
                sensordict[sen] = None
            iddict = dict([('Measures', dict()), ('ForcePUB', True)])
            mainbuffer = dict()
            for senid in ZWAVE_NODEID_LIST:
                mainbuffer[senid] = copy.deepcopy(iddict)

    return mainbuffer


def get_sensors(senid, measureapi):
    """CACHED OPERATION
    This function will fetch the sensor values that are stored in the buffer instead of issuing a waiting the zwave.
    It is responsibility of daemon to have them uptodate"""
    global mainbuffer
    if isinstance(senid, str):
        senid = int(senid)
    if senid not in ZWAVE_NODEID_LIST and senid is not None:
        logger.warning('Zwave id ' + str(senid) + ' not found in defined list')
        return False
    else:
        try:
            toret = (mainbuffer[senid]['Measures'][SENSOR_API_TO_INT[measureapi]]['Time'], mainbuffer[senid]['Measures'][SENSOR_API_TO_INT[measureapi]]['Value'])
            return toret
        except KeyError:
            logger.error('Not supported measure type: {0}, or not yet register an initial measure'.format(SENSOR_API_TO_INT[measureapi]))
            return False


def processframe(frame):
    global mainbuffer, zwavePUBsocket, SENSOR_INT_TO_API
    timeofevent = frame[0]
    senid = frame[1]  # This is an uint8
    data = frame[2]
    if not senid in ZWAVE_NODEID_LIST:
        logger.info('Node {0} is not expected by the collector'.format(senid))
        return
    sensors_stat[senid] = True  # Register that we have a packet of this node
    changeflag = False # Publish only on change, raise changed flag for console "*" print
    if 'Sensor Type' in data and data['Sensor Type'] in ZWAVE_SUPPORTED_SENSORS:
        sensornameinternal = data['Sensor Type']
        # Fixing the PIR from boolean to int to plot it as points
        if sensornameinternal == 'PIR':
            if data['Value']:
                sensorvalue = 1
            else:
                sensorvalue = 0
        else:
            sensorvalue = float(data['Value'])
        sensornameapi = SENSOR_INT_TO_API[sensornameinternal]  # Change to general API for storing and publishing
        measure = {'Time': timeofevent, 'Value': sensorvalue}
        try:
            #Primary protection from corrupted sensor measure, compare absolute range
            if measure['Value'] < ZWAVE_SENSORS_VALID_RANGE[sensornameinternal][0] or measure['Value'] > ZWAVE_SENSORS_VALID_RANGE[sensornameinternal][1]:
                return
            #Secondary protection from corrupted sensor measure, compare deltas
            if abs(measure['Value'] - mainbuffer[senid]['Measures'][sensornameinternal]['Value']) > ZWAVE_SENSORS_ERROR_DELTA[sensornameinternal]:
                return
            #Check if we have a change in value
            if mainbuffer[senid]['ForcePUB'] or \
                            abs(measure['Value'] - mainbuffer[senid]['Measures'][sensornameinternal]['Value']) \
                            >= ZWAVE_SENSORS_RESOLUTION[sensornameinternal]:
                submsg = {'MID_ID': ZWAVE_MID_ID,
                          'REF': senid,
                          'MSG_TYPE': PUB_MSG_TYPE[1],
                          'PAYLOAD': {'Quantity': sensornameapi, 'Time': measure['Time'], 'Value': measure['Value']}}
                zwavePUBsocket.send_json(submsg)
                changeflag = True
                mainbuffer[senid]['ForcePUB'] = False
            mainbuffer[senid]['Measures'][sensornameinternal] = measure  # keep the buffer up to date at all time
        except KeyError:  # in case of a new quantity in buffer
            submsg = {'MID_ID': ZWAVE_MID_ID,
                          'REF': senid,
                          'MSG_TYPE': PUB_MSG_TYPE[1],
                          'PAYLOAD': {'Quantity': sensornameapi, 'Time': measure['Time'], 'Value': measure['Value']}}
            zwavePUBsocket.send_json(submsg)
            mainbuffer[senid]['Measures'][sensornameinternal] = measure
            mainbuffer[senid]['ForcePUB'] = False
            changeflag = True
        if logger.getEffectiveLevel() <= 20:  # 20 is the level of Info, do a precheck before running uselessly the function
            printresults(senid, data, changeflag)
        return True  # Successful
    else:
        logger.warning('NodeID {0:d} unknown packet ignored: {1}'.format(senid, data))


def printresults(nodeid, info, changeflag=False):
    try:  # Custom make debug representation of the samples
        if info['Command'] == 'SENSOR_MULTILEVEL_REPORT':
            msgstr = "NodeID {0:d}".format(nodeid)
            if info['Sensor Type'] == 'Air temperature':
                # I remove 1 degree for display purposes and calibration
                msgstr += " Sensor Type: {0:s}  Value: {1:.2f} {2:s}".format(info['Sensor Type'], float(info['Value']) - 1,
                                                                        info['Unity'])
            else:
                msgstr += " Sensor Type: {0:s}  Value: {1:.2f} {2:s}".format(info['Sensor Type'], float(info['Value']),
                                                                        info['Unity'])
        elif info['Command'] == 'BATTERY_REPORT':
            msgstr = "NodeID {0:d}".format(nodeid)
            msgstr += " {0:s}: {1:d}{2:s}".format(info['Sensor Type'], info['Value'], info['Unity'])
        elif info['Command'] == 'SENSOR_BINARY_REPORT':
            if info['Value']:
                msgstr = "NodeID {0:d}".format(nodeid)
                msgstr += " Motion detected"
            else:
                msgstr = "NodeID {0:d}".format(nodeid)
                msgstr += " NO Motion detected for the configured sensor timeout"
        elif info['Command'] == 'THERMOSTAT_SETPOINT_REPORT':
            msgstr = "NodeID {0:d} ".format(nodeid)
            msgstr += info['Sensor Type'] + 'Setting is:' + info['Value'] + info['Unity']
        elif info['Command'] == 'WAKE_UP_NOTIFICATION':
            msgstr = "NodeID {0:d}".format(nodeid)
            msgstr += " Sent a wake up packet"
        else:
            msgstr = "NodeID {0:d}".format(nodeid)
            msgstr += info['Command Class'] + info['Command']
        if changeflag:  # Output shows change in value with the *
            logger.info(msgstr + ' * ')
        else:
            logger.info(msgstr)
    except KeyError as e:  # In debug print what node made a key exception, and print whole packet
        logger.exception(repr(e))
        logger.error("NodeID " + str(nodeid))
        logger.error(str(info))


def execute_zwave_cmd(message, zwaveapi, asyncevent):
    """The interpretation of the requested DIRECT ZWAVE command is here
     most probably it will be just to read the zwave. Message is (ID, ACTION). ID can be None but should be int otherwise.
    """
    global mainbuffer, sensors_stat
    cmd = message[1]
    if message[0] != 0xFFFF:  # 0xFFFF is the empty plcid async thread
        if isinstance(message[0], str):
            zwid = int(message[0])
        else:
            zwid = message[0]
        if zwid not in ZWAVE_NODEID_LIST:
            logger.warning('Zwave id ' + str(zwid) + ' not found in defined list')
            return False
        logger.debug("Received ZWAVE command: " + cmd + "by zw: " + str(zwid))
    else:
        zwid = 0xFFFF

    if cmd == 'WAKE':
        return zwaveapi.send_primary_wake()

    elif cmd == 'ASYNCZWAVE':
        if not asyncevent.isSet():
            return False  # The trigger was by a sync packet, return to the async_zwave_thread()
        decodeddata = zwaveapi.get_network_message()
        if decodeddata:
            processframe(decodeddata)
            return True
        else:
            return False

    elif cmd == 'SYNCZWAVE':
        #With SYNCZWAVE we consider the remote sensor is listening.
        measureapi = message[2]
        measureint = SENSOR_API_TO_INT[measureapi]
        logger.warning('Got SYNCZWAVE req for id {0} and sensor {1} '.format(zwid, measureint))
        frame = zwaveapi.get_multisensor_values(nodeid=zwid, measure=measureint, syncbybutton=True, nodelistening=True)
        if frame:
            res = processframe(frame)  # Buffer is updated here
        else:
            return False
        if res:
            #we use the generic func to get it from the updated buffer
            return get_sensors(frame[1], measureapi)
        else:
            return False
    else:
        logger.info('Command not defined for ZWAVE cmd: ' + cmd)
        return False


def execute_daemon_cmd(message):
    """The interpretation of the requested Daemon command is here
     most probably it will be just to read the zwave. Message is (ID, ACTION).
    """
    global mainbuffer, sensors_stat, zwavePUBsocket
    if message[0]:
        if isinstance(message[0], str):
            zwid = int(message[0])
        else:
            zwid = message[0]
        if zwid not in ZWAVE_NODEID_LIST:
            logger.warning('Zwave id ' + str(zwid) + ' not found in defined list')
            return False
    else:
        logger.warning('No Zwave ID is given')
        return False  # Id cannot be NONE for this function

    cmd = message[1]
    measureapi = message[2]
    logger.debug("Received Daemon command: " + cmd + "by zw: " + str(zwid))

    if cmd == 'SENSOR':
        return get_sensors(zwid, measureapi)
    else:
        logger.info('Command not defined for Daemon cmd: ' + cmd)
        return False


#------------------------------------------------Threads Definitions---------------------------------------------------#
def async_zwave_thread(zwaveapi, asyncevent):
    """This is for the async power collect thread that should always listen for packets."""
    global mainbuffer, sensors_stat
    begintime = time.time()
    begintime2 = time.time()
    begintime3 = time.time()
    while True:
        asyncevent.wait()
        if time.time() - begintime > 900:
            execute_zwave_cmd((0xFFFF, 'WAKE'), zwaveapi, asyncevent)
            begintime = time.time()  # update the "watchdog" timer
        if time.time() - begintime3 > 1800:
            # Reset sensor values and check if one is dead
            begintime3 = time.time()
            for key in sensors_stat:
                if not sensors_stat[key]:
                    logger.error('NodeID {0} is not responding for the last 30min'.format(key))
                sensors_stat[key] = False
        if time.time() - begintime2 > 3600:
            #Force resend of PUB message once in a while in case datacollector or websocket missed it
            begintime2 = time.time()
            createorread_buffer(forcepub=True)

        #wait for incomming packet
        zwaveapi.uartapi.block_and_wait_byte(waittime=-1)
        # res is raw data from zwave. mainbuffer has the data interpretted
        res = execute_zwave_cmd((0xFFFF, 'ASYNCZWAVE'), zwaveapi, asyncevent)
        if res:
            save_buffer()
#------------------------------------------------END Threads Definition---------------------------------------------------#


def exit_gracefully(_signo, _stack_frame):
    """Terminates the sockets gracefully without any left hanging open."""
    logger.info('Closing sockets and exiting gracefully')
    if zwaveREPsocket:
       zwaveREPsocket.setsockopt(zmq.LINGER, 0)
       zwaveREPsocket.close()
    if zwavePUBsocket:
       zwavePUBsocket.setsockopt(zmq.LINGER, 0)
       zwavePUBsocket.close()
    if context:    
       context.destroy()
    save_buffer()
    sys.exit(0)


if __name__ == "__main__":

    logger = loggercolorizer.getColorLogger(level=LOGGER_LVL, activate_email_logging=True, email_sender='ZWAVEdaemon@{0}'.format(CURRENT_IP))

    signal.signal(signal.SIGINT, exit_gracefully)
    signal.signal(signal.SIGTERM, exit_gracefully)
    if platform.system() == 'Linux':
        signal.signal(signal.SIGHUP, exit_gracefully)
        signal.signal(signal.SIGQUIT, exit_gracefully)

    middleware = get_middleware_details(midid=ZWAVE_MID_ID) #Execution will stop if it fails to find openBMS
    if middleware['middleware_type'] !='ZWAVE':
        logger.error('Could not find the ZWAVE middleware in openBMS, verify the ID in conf_zwave.py')
        sys.exit()
    ip = middleware['IP']
    rep_port = middleware['REP_port']
    pub_port = middleware['PUB_port']
    logger.info('Starting the middleware "{0}" @ "{1}:{2}/{3}"'.format(middleware['name'], ip, rep_port,pub_port))
    context = zmq.Context()
    zwaveREPsocket = context.socket(zmq.REP)
    zwaveREPsocket.bind('tcp://' + ip + ':' + rep_port)
    zwavePUBsocket = context.socket(zmq.PUB)
    zwavePUBsocket.bind('tcp://' + ip + ':' + pub_port)
    ZWAVE_NODEID_LIST = get_middleware_refs(midid=ZWAVE_MID_ID)
    mainbuffer = createorread_buffer()
    #Convert all the internal zwave representation to api type for use by PUB
    SENSOR_INT_TO_API = dict()
    SENSOR_API_TO_INT = dict()
    if len(ZWAVE_SUPPORTED_SENSORS) != len(SENSORS_API_TYPES):
        logger.warning('There is a mismatch between sensor supported')
    for internal, api in zip(ZWAVE_SUPPORTED_SENSORS, SENSORS_API_TYPES):
        SENSOR_INT_TO_API[internal] = api
        SENSOR_API_TO_INT[api] = internal

    serialapi = uart.SerialInterface(usbconf=ZWAVE_USB_PARAM)
    ret = serialapi.connect()
    if ret:
        zwaveapi = AeotecApi(uartapi=serialapi)
        sensors_stat = dict(zip(ZWAVE_NODEID_LIST, [False] * len(ZWAVE_NODEID_LIST)))  # online status of sensors
    else:
        logger.error("Device error, exiting")
        sys.exit()

    #Initialize the wakeuo beginning procedure
    zwaveapi.send_primary_soft_reset()
    if not execute_zwave_cmd((0xFFFF, 'WAKE'), zwaveapi, None):
        sys.exit()  # In this case no real communication achieved

    #Verify connected modules
    modulesdiscovered = zwaveapi.get_node_list()
    if 1 in modulesdiscovered: modulesdiscovered.remove(1) #Remove the static controller
    for nd in modulesdiscovered:
        if nd not in ZWAVE_NODEID_LIST:
            logger.warning('ID {0} not in openBMS list'.format(nd))
    for zwid in ZWAVE_NODEID_LIST:
        if zwid not in modulesdiscovered:
            logger.warning('OpenBMS Ref {0} not found in the zwave network'.format(zwid))

    # ---------Multiple threads preparation---------#
    asyncevent = threading.Event()
    mainevent = threading.Event()  # This signals that event can proceed, sync power finished current work.
    asyncthread = threading.Thread(target=async_zwave_thread, args=(zwaveapi, asyncevent))

    asyncthread.start()
    asyncstate = True

    #Specify the routing criterions
    daemon_commands = ['SENSOR']
    zwave_commands = ['WAKE'] + ['ASYNCZWAVE'] + ['SYNCZWAVE']

    logger.info('Zwave daemon has started')
    while True:
        if asyncstate:
            asyncevent.set()  # set it only if is already set(in case sync thread is still working)
        try:
            zmqmessages = zwaveREPsocket.recv_json()
            #normalising to list. Since Json doesnt support tuple but even a single zmqmessage(the tuple) will be a list,
            #we are making sure the first element to be list)
            reqislist = True
            if not isinstance(zmqmessages[0], list):
                zmqmessages = [zmqmessages]
                reqislist = False
            res = []
            for zmqmessage in zmqmessages:
                zmqmessage[1] = zmqmessage[1].upper()
                if zmqmessage[1] in daemon_commands:
                    res0 = execute_daemon_cmd(zmqmessage)
                    res.append(res0)
                elif zmqmessage[1] in zwave_commands:
                    #directly execute in zwave network, buffer is involved only for updating it.
                    #this case is for a remote that wants ASYNCZWAVE
                    try:
                        asyncstate = asyncevent.isSet()
                        asyncevent.clear()
                        res0 = execute_zwave_cmd(zmqmessage, zwaveapi, asyncevent)
                        res.append(res0)
                    finally:
                        if asyncstate:  # Free again the async thread
                            asyncevent.set()
                    if res0:  # If there is a change update our buffer store
                        #CAUTION this writes constanlty to IO device. At when using normal console the  atexit callback would
                        #do it only once. At production remove this save. Also all the saves done in threads' functions
                        save_buffer()
                else:
                    logger.warning('Unknown command received: {0}'.format(zmqmessage[1]))
            if reqislist:
                zwaveREPsocket.send_json(res)
            else:
                zwaveREPsocket.send_json(res[0])
        except Exception as e:
            logger.exception(repr(e))
            zwaveREPsocket.send_json(False)  #Register the failure with false without breaking the loop/daemon