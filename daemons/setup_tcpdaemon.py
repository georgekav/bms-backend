import sys

from cx_Freeze import setup, Executable

includefiles = ['addsleepertask.bat']
includes = ['os', 'socket', 'zmq.backend.cython']
excludes = ['_gtkagg', '_tkagg', 'bsddb', 'curses', 'email', 'pywin.debugger',
            'pywin.debugger.dbgcon', 'pywin.dialogs', 'tcl',
            'Tkconstants', 'Tkinter', 'scipy', 'numpy', '_ssl', 'unittest', 'xml', 'unicodedata', '_hashlib', 'gevent',
            'tornado', 'pyreadline', 'nose', 'logging', 'json', 'doctest', 'urllib',
            'urllib']
packages = []
sys.path.append('../../libs/')
sys.path.append('../../libs/zmq')
sleepexedir = 'C:/work/elab-bms/builds/Sleep_controller'
if sys.platform == "win32":
    base = "Win32GUI"
else:
    base = "Console"
setup(
    name="PC Sleeper",
    version="0.1",
    description="A pc sleep controller",
    options={'build_exe': {'build_exe': sleepexedir, 'excludes': excludes, 'includes': includes, 'packages': packages,
                           'include_files': includefiles}},
    executables=[Executable(
        script="tcpdaemon.py",
        base=base,
        initScript=None,
        compress=True,
        copyDependentFiles=True,
        appendScriptToExe=False,
        appendScriptToLibrary=False,
    )])