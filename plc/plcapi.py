__author__ = 'Georgios Lilis'
import logging
import time

from conf_plc import PLC_REQ_PAYLOAD, RELAY_CMD, SYNCPLC_CMD, DIMMER_CMD, PLC_CONFIG_CMD
from bitstring import ConstBitStream
from plc.plcinterface import PlcInterface


class EsmartApi(PlcInterface):
    """This class provides all the high level API to interface direct the functionalities of the esmart plug.
    The only required argument is the plc interface"""

    def __init__(self, uartapi):
        super(EsmartApi, self).__init__(uartapi=uartapi)
        if not self.status:
            raise RuntimeError('PLC interface not ready, check uart device status')

    def req_sync_powers(self, plcch):
        """Reads the powers using Gilberts functionality. Returns S PF P Q"""
        plcch = self.dev_addr_normalizer(plcch)
        payload = PLC_REQ_PAYLOAD[SYNCPLC_CMD]
        response = self.plc_request(plcid=plcch, msgtype='Command', payload=payload)
        if response:
            currenttime = time.time()  # We register as soon as possible at arrival
            payload = self.return_payload(response)
            if payload:
                powers = self.decode_payload(payload)
                if powers:
                    powers['Time'] = currenttime
                    return powers
                else:
                    return False

    def wait_asynq_frame(self, interrupt):
        """Blocks with timeout and waits the asynq frame from esmart. The interrupt will be passed in the uartapi and it essential
        if we want to immediatly stop waiting before the timeout or a packet arrival and return. Interrupt must be a handle,
        when it is signaled the block_and_wait will return."""
        # wait for the first time, this means plc is busy
        interrupt.wait()
        # green light to proceed waiting for sample
        self.uartapi.block_and_wait_byte(waittime=-1)
        while not interrupt.isSet():  # In case a plc command is requested and triggers the uart we wait until is free.
            interrupt.wait()  # We block in the mean time
            self.uartapi.block_and_wait_byte(waittime=-1)
        packet = self._get_response()
        if packet:
            decodedframe = self.decodeFrame(packet)
            if not decodedframe:
                return False
            msg_type = decodedframe[0]
            addr = decodedframe[1]
            decoded_payload = decodedframe[2]
            plcid = int(addr,16)
            return msg_type, plcid, decoded_payload
        else:
            #It will enter here if this thread is freed from block and wait due to some other thread plc activity
            #but no particular reply is waiting
            return False

    def set_relay_status(self, plcid, action):
        """Action can be 'ON', 'OFF', 'TOGGLE', and id one of the plugs."""
        plcid = self.dev_addr_normalizer(plcid)
        if not action in RELAY_CMD:
            logging.warning('Unknown plc relay action: ' + action)
            return False
        payload = PLC_REQ_PAYLOAD[action]
        #WORKAROUND
        if action == RELAY_CMD[2]:
            send_times=1 #Otherwise it may trigger twice...
        else:
            send_times=2
        res = self.plc_request(plcid=plcid, msgtype='Command', payload=payload, send_times=send_times, norep=True)
        return res

    def set_dimmer_status(self, plcid, dimsetting):
        """Dimsetting a int value 0 -> 100"""
        plcid = self.dev_addr_normalizer(plcid)
        if dimsetting < 0 or dimsetting > 100:
            logging.warning('Wrong dimmer setting is given')
            return False
        dimsettingint = dimsetting * 1024 / 100
        dimsettinghex = format(dimsettingint, '04X')
        payload = PLC_REQ_PAYLOAD[DIMMER_CMD] + ConstBitStream(hex=dimsettinghex).bytes
        res = self.plc_request(plcid=plcid, msgtype='Command', payload=payload, send_times=2, norep=True)
        return res

    def check_master_plug(self):
        """Check if master in reachable"""
        payload = PLC_REQ_PAYLOAD['FirmwareREQ']
        for master_id in [0x00, 0xb0]:
            status = "Probing master ID {0}".format(master_id)
            discovered = False
            frame = self.plc_request(plcid=master_id, msgtype='Command', payload=payload, send_times=1)
            if frame:
                msgtype, repplcid, values = self.decodeFrame(frame)
                if values[0][0] == "Firmware ID" and repplcid == '{0:04X}'.format(master_id):
                    discovered = True
            if discovered:
                status += ': Found'
                logging.info(status)
                break
            else:
                status += ': Missing'
                logging.info(status)
        return discovered

    def req_subnet_nodes(self, maxaddress=0xff):
        """Ask all the nodes in the subnet, maxaddress for the max plc id to scan starting from module 0"""
        nodelist =[]
        for moduleID in range(0, maxaddress+1, 4):
            payload = PLC_REQ_PAYLOAD['FirmwareREQ']
            status = "Probing slave ID {0}".format(moduleID)
            discovered = False
            frame = self.plc_request(plcid=moduleID, msgtype='Command', payload=payload, send_times=1)
            if frame:
                msgtype, repplcid, values = self.decodeFrame(frame)
                if values[0][0] == "Firmware ID" and repplcid == '{0:04X}'.format(moduleID):
                    nodelist.append(moduleID)
                    discovered = True
            if discovered:
                status += ': Found'
            else:
                status += ': Missing'
            logging.info(status)

        logging.info('Discovered nodes {0}'.format(nodelist))
        return nodelist

    def get_conf_node(self, plcid, start_addr_eeprom, num_bytes):
        """GET EEPROM configuration from the given plugID from the starting EEPROM adress (not the full adress space address)
        and the number of bytes to read. PlugID is ch0 of course"""
        plcid = self.dev_addr_normalizer(plcid)
        payload = PLC_REQ_PAYLOAD['ConfREQ'] + ConstBitStream(int=start_addr_eeprom, length=16).bytes + PLC_CONFIG_CMD['GET'] + ConstBitStream(int=num_bytes, length=8).bytes
        packet = self.plc_request(plcid=plcid, msgtype='Config', payload=payload)
        res_type, ret_plcid, decodedframe = self.decodeFrame(packet)
        if ret_plcid == format(plcid, '04X') and res_type=='Parameter':
            logging.info('GET from EEPROM of plug {0:04X}: At {1}: {2} --- {3}: {4}'.format(plcid, decodedframe[0][0], decodedframe[0][1], decodedframe[1][0], decodedframe[1][1]))
            return decodedframe[1][1] #this is the EEPROM read bytes
        else:
            logging.info('GET from EEPROM of plug {0:04X}: FAILED - RETRYING'.format(plcid))
            self.get_conf_node(plcid, start_addr_eeprom, num_bytes)

    def set_conf_node(self, plcid, start_addr_eeprom, byte_to_write):
        """SET EEPROM configuration to the given plugID from the starting EEPROM adress (not the full adress space address)
        and the byte to write (ONLY ONE NOW due to fixed ConfREQ length). PlugID is ch0 of course"""
        plcid = self.dev_addr_normalizer(plcid)
        payload = PLC_REQ_PAYLOAD['ConfREQ'] + ConstBitStream(int=start_addr_eeprom, length=16).bytes + PLC_CONFIG_CMD['SET'] + ConstBitStream(hex=byte_to_write).bytes
        res = self.plc_request(plcid=plcid, msgtype='Config', payload=payload, norep=True)
        if res:
            byte_read = self.get_conf_node(plcid, start_addr_eeprom, 1)
            if byte_read == byte_to_write:
                logging.info('SET to EEPROM of plug {0:04X}: At Address: {1:04X} --- Byte: {2}'.format(plcid, start_addr_eeprom, byte_to_write))
                return True
            else:
                logging.info('SET to EEPROM of plug {0:04X}: FAILED - RETRYING'.format(plcid))
                self.set_conf_node(plcid, start_addr_eeprom, byte_to_write)


#Example to set configuration
# from uart import SerialInterface
# from conf_plc import PLC_USB_PARAM
# logging.root.setLevel(logging.INFO)
# serialapi = SerialInterface(usbconf=PLC_USB_PARAM)
# ret = serialapi.connect()
# if ret:
#     esmartapi = EsmartApi(uartapi=serialapi)
# while True:
#     esmartapi.get_conf_node(0x00b0, 0, 10)