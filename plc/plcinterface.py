from __future__ import division
__author__ = 'Georgios Lilis'

from conf_plc import *
from uart import UartTimeoutError
import math
import crcmod
import logging
import time
from bitstring import ConstBitStream
from array import array


class PlcInterface(object):
    """This class is necessary for handling all the communication with the plc plugs. It the final
    interface before the plc protocol. To send a message, first create_e5_msg, then send_msg.
    To read the channel run wait_response"""

    def __init__(self, uartapi):  # devAddr and devChannel in hex of len 2
        self._finalMSG = None
        self.uartapi = uartapi
        if self.uartapi.device:
            self.status = True
        else:
            self.status = False

    @property
    def final_msg(self):
        return self._finalMSG

    @final_msg.setter
    def final_msg(self, value):
        self._finalMSG = value

    def _create_e5_msg(self, plcid, msgtype, payload):
        """Message can be created by giving directly the end channel, msgtype and payload,
        msgtype and payload should be in hex bytes"""
        if not msgtype:
            logging.warning("Message type is not defined, message cannot be created")
            return
        if not payload:
            logging.warning("Payload is not defined, message cannot be created")
            return
        # Accept universal data format, either str or int
        if not isinstance(plcid, int):
            logging.warning("plcid is not an int number, attempting to parse the hex string")
            plcid = self.dev_addr_normalizer(devaddr=plcid)
        if plcid > 0xFFFF:
            raise ValueError("The plc id is wrong, >0xFFFF")
        # Check if it is a predifined payload name
        if msgtype in PLC_REQ_MSG_TYPE:
            msgtype = PLC_REQ_MSG_TYPE[msgtype]
        msg_length = len(payload) + len(msgtype) + len(ConstBitStream(uint=plcid, length=16).bytes)
        message_before_crc = HEADER_E5 + VERSION + ConstBitStream(uint=msg_length, length=8).bytes + \
            msgtype + ConstBitStream(uint=plcid, length=16).bytes + payload
        crcvalue = self.get_crc(message_before_crc)
        message_after_crc = message_before_crc + crcvalue

        self._finalMSG = message_after_crc
        return ConstBitStream(bytes=message_after_crc).hex

    def _transmit_msg(self):
        if not self._finalMSG:
            logging.warning("No message defined, create one with .create_MSG_E5")
            return
        if self.status:
            received = False
            tries = 0
            while not received and tries<=3:
                self.uartapi.write(self._finalMSG)
                logging.debug("SEND %02d:%02d:%02d %s "
                      % (time.localtime()[3], time.localtime()[4], time.localtime()[5], ConstBitStream(bytes=self._finalMSG).hex))
                tries += 1
                response = self._get_ack_nack()
                if response:
                    logging.debug("ACK")
                    return "ACK"
                elif not response:
                    logging.debug("NACK")
        else:
            logging.warning("Uart api not ready, nothing sent")
            return

    def _get_ack_nack(self):  # it does not get out of here without ACK/NACK or <WAIT_TIME
        """The function to check if the message arrived to the master plc interface and get its acknowledgement.
        It doesnt check if the message arrived to final destination. This is api specific and not inteface specific"""
        try:
            read_msg = self.uartapi.read(4)
            if read_msg[0] == HEADER_ACK:
                return True  # "ACK"
            elif read_msg[0] == HEADER_NACK:
                return False  # "NACK"
        except UartTimeoutError:
            logging.debug("No responce")

    def _get_response(self):
        """Methods for acquiring requested data or listen for packet and checking crc in the meanwhile"""
        if not self.status:
            logging.warning("Uart api not ready")
            return
        read_msg = ""
        try:
            if self.uartapi.read(1) == HEADER_E5:
                temp_msg = self.uartapi.read(2)  # read two bytes
                length = ConstBitStream(bytes=temp_msg[1]).uint
                crclength = 1
                payload = self.uartapi.read(length + crclength)
                if len(payload) == length + crclength:
                    read_msg = HEADER_E5 + temp_msg + payload
                    if not self.check_crc(read_msg):
                        logging.debug("CRC check failed")
                        self.uartapi.write(NACK)
                        self._get_response()
                logging.debug("RECEIVE %02d:%02d:%02d %s "
                      % (time.localtime()[3], time.localtime()[4], time.localtime()[5], ConstBitStream(bytes=read_msg).hex))
            return read_msg
        except UartTimeoutError:
            logging.debug("No data to read, timeout")

    def plc_request(self, plcid, msgtype, payload, send_times=1, repet_times=3, norep=False):
        """Do a full PLC request, with the ACK/NACK verification and all the nessasary repetitions to guaranty correct
        reception.
        send_times: initial transmition times
        repet_times: are how many times will attempt to repeat (ACK successfull) message and response reading
        norep: is True, we dont expect a reply from the PLC
        """
        self._create_e5_msg(plcid=plcid, msgtype=msgtype, payload=payload)
        if not norep:
            rep = False
            watchdog = 1
            while not rep:
                if watchdog > repet_times:
                    return False
                for i in range(int(send_times)):
                    self._transmit_msg()
                rep = self._get_response()
                watchdog += 1
            return rep
        else:
            for i in range(int(send_times)):
                self._transmit_msg()
            return True

    def decode_payload(self, payload):
        """We take based on the payload hex number the characteristics of this sensor
        This characteristics are returned by Type_Payload by giving the hex number of the payload
        We have for example Sensor_pwrof10_len patterned returned by Type_Payload
        Then we split it to payload sensor packets, and interpret each packet .
        Returns a list of dictionaries with keys the value name, and dict value the SI value of measure"""
        if not payload:
            logging.debug("Payload empty, skipping")
            return

        #Concert it to ConstBitStream from bytes
        payload = ConstBitStream(bytes=payload)
        byteindex = 0
        payloadtyperaw = payload.read('bytes:1')
        if not payloadtyperaw in PLC_REP_PAYLOAD:  # Check just by the key
            logging.warning('Unrecognized payload type in payload: {0}'.format(payload.hex))
            return
        elif self.return_payload_type(payloadtyperaw) is PLC_ACK:
            #Skipping the ACK packet from plug action confirmation
            return
        else:
            payloadtype = self.return_payload_type(payloadtyperaw)
            payloadtypesplited = payloadtype.split("_")
            valuename = payloadtypesplited[0]
            valuemultiplier = payloadtypesplited[1]
            valuesize = int(payloadtypesplited[2], 10)

        powerstoret = {}  # Create dictionary with just the time
        if valuename == "Spf":  # We break the tuple to the dict
            rawvalue = payload.read('bytes:'+str(valuesize))
            temptuple = self.extract_powers(rawvalue)  # It accepts payload in bytes
            powerstoret["P"] = temptuple[0]
            powerstoret["Q"] = temptuple[1]
            powerstoret["PF"] = temptuple[2]
            powerstoret["S"] = temptuple[3]
        else:  # all the rest types need just a multiplier
            rawvalue = payload.read('uint:'+str(valuesize * 8))
            powerstoret[valuename] = rawvalue * 10 ** int(valuemultiplier)
        return powerstoret

    @staticmethod
    def get_crc(msg):
        crc_fun = crcmod.mkCrcFun(0x131, initCrc=0, rev=False, xorOut=0)
        crcres = ConstBitStream(uint=crc_fun(msg), length=8).bytes
        return crcres

    @staticmethod
    def check_crc(msg, crc_code=""):
        if not crc_code:
            crc_code = msg[-1]
        crc_fun = crcmod.mkCrcFun(0x131, initCrc=0, rev=False, xorOut=0)
        crcres = ConstBitStream(uint=crc_fun(msg[:-1]),
                           length=8).bytes  # We take from the beginning until 1 !byte! before last
        return crcres == crc_code

    @staticmethod
    def return_address(response, datatype="hex"):
        """Parse the address in hex format, like the default in class."""
        if response >= 6:
            tempaddress = response[4:6]  # address start at 6 position, 3 for ID+msgtype
            if datatype == "int":
                address = ConstBitStream(bytes=tempaddress).uint
                return address
            elif datatype == "hex":
                return ConstBitStream(bytes=tempaddress).hex
        else:
            logging.error("Cannot parse address, message is invalid")
            return None

    @staticmethod
    def return_msg_type(response, datatype="hex"):
        """Parse the messge type in int format, like the default in class."""
        if datatype == "int":
            address = ConstBitStream(bytes=response[3]).uint
            return address
        elif datatype == "hex":
            return ConstBitStream(bytes=response[3]).hex


    @staticmethod
    def return_payload(response):
        """Parse just the payload, returned in bytes"""
        if response >= 6:
            length = ord(response[2])
            payload = response[6:6 + (length - 3)]  # Payload start at 6 position, 3 for ID+msgtype
            return payload
        else:
            logging.error("Cannot parse payload, message is invalid")
            return None

    @staticmethod
    def return_payload_type(payload):
        return PLC_REP_PAYLOAD[payload[0]]  # It is the first byte

    @staticmethod
    def dev_addr_normalizer(devaddr):
        """This function takes the devAddr and always returns it in int format required by the
        class functions. It should always called when devAddr is comming from outside the class"""
        if isinstance(devaddr, int):
            # Do nothing, it is correct
            pass
        elif isinstance(devaddr, str):  # Hex string,
            devaddr = int(devaddr, 16)
        return devaddr

    @staticmethod
    def extract_powers(s_pf):
        """Takes S + pf payload in bytes and renders the tuble with the (P, Q, pf)
        The packet is in the form: P0 P1 P2 P3 A0 A1 #Px is the S, Ax is the pf
        S is in mVA and pf in millidegrees.
        LITTLE endian is the default"""
        s_bytes = s_pf[0:4]
        pf_bytes = s_pf[4:6]
        sraw = ConstBitStream(bytes=s_bytes).uintle
        pfraw = ConstBitStream(bytes=pf_bytes).uintle
        s = sraw / 1000
        pf = pfraw / 1000
        pfradias = math.radians(pf)
        cosine_pf = math.cos(pfradias)
        sine_pf = math.sin(pfradias)
        p = round(s * cosine_pf, 3)
        q = round(s * sine_pf, 3)
        pf = round(cosine_pf, 3)
        p_q_pf_s = (p, q, pf, s)
        return p_q_pf_s

    @staticmethod
    def decodeFrame(frame):
        frame_data = frame[3:-1]
        data_type = ord(frame_data[0])
        frametype_str = PLC_DATA_TYPES[data_type]
        sender_id = array('H', frame_data[1:3])
        sender_id.byteswap()
        senderid_str = "%04X" % sender_id[0]
        fields = getFields(frame_data[3:], data_type)
        decoded_values = []
        for key, value in fields:
            if data_type in [0x01, 0x02, 0x03]:
                if key == 0x0A:
                    decoded_values.append(("Temperature 1", (value - 0x3200)/100.))
                elif(key == 0x0B):
                    decoded_values.append(("Temperature 2", (value - 0x3200)/100.))
                elif(key == 0x0C):
                    decoded_values.append(("Temp.   Delta", (value - 0x3200)/100.))
                elif(key == 0x12):
                    decoded_values.append(("Target  Temp.", (value - 0x3200)/100.))
                elif(key == 0x07):
                    decoded_values.append(("Power (W)", (value)))
                elif(key == 0x08):
                    decoded_values.append(("Power (kW)", (value / 10.)))
                elif(key == 0x10):
                    decoded_values.append(("Flow (dm^3/h)", (value)))
                elif(key == 0x16):
                    decoded_values.append(("Version Number", (value % 256 / 100. + value / 256)))
                elif(key == 0x40):
                    decoded_values.append(("Energy (Wh)", (value)))
                elif(key == 0x42):
                    decoded_values.append(("Volume (dm^3)", (value / 10.)))
                elif(key == 0x18):
                    decoded_values.append(("Scenes version", "%d" % value))
                elif(key == 0x56):
                    firmware_id = PLC_FIRMWARES[(value & 0xFF000000) >> 24]
                    value &= 0xFFFFFF
                    firmware_ver = "%d.%d.%d" % (value / 0xFF00,
                                                 value % 0x10000 / 0xFF,
                                                 value % 0x100)
                    decoded_values.append(("Firmware ID", "%s v%s" % (firmware_id, firmware_ver)))
                elif(key == 0x57):
                    firmware_id = PLC_BOOTLOADERS[(value & 0xFF000000) >> 24]
                    value &= 0xFFFFFF
                    firmware_ver = "%d.%d.%d" % (value / 0xFF00,
                                                 value % 0x10000 / 0xFF,
                                                 value % 0x100)
                    decoded_values.append(("Bootloader ID", "%s v%s" % (firmware_id, firmware_ver)))
                elif key == 0x01:  # get measurement
                    decoded_values.append(("Get measurement", PLC_UNITS.get(value, (2, 'N/A'))[1]))
                elif key == 0x03:  #RELAY
                    decoded_values.append((PLC_ACTUATOR_TYPES[0], value))
                elif key == 0x04:  #DIMMER, it is at 1000 and we make it max 100
                    decoded_values.append((PLC_ACTUATOR_TYPES[1], int(value/10)))
                else:
                    decoded_values.append((PLC_UNITS.get(key, (2, 'N/A'))[1], value))
            if(data_type == 0x06):
                if(key == 0x25):
                    decoded_values.append((PLC_ERRORS[key][1],
                                           FLASH_ERRORS[value]))
                else:
                    decoded_values.append((PLC_ERRORS.get(key, (2, 'N/A'))[1], value))
            if(data_type == 0x04):
                if(key == 0x61): #config response
                    start_addr = value[:4]
                    bytes_read = value[4:]
                    decoded_values.extend([("Start Address", start_addr), ("Read Bytes", bytes_read)])
            if(data_type == 0x08):
                if(key == 0x3E):
                    if value == '0087': #removal of load
                        decoded_values.append(("Load RFID", None))
                    else:
                        decoded_values.append(("Load RFID", value))
        return frametype_str, senderid_str, decoded_values

def str_to_int(s):
    sum = 0
    for i in range(len(s)):
        sum += ord(s[i]) << (8 * (len(s)-i-1))
    return sum

def lookup_fields(data, lookup):
    #logging.debug(["%02X" % ord(c) for c in data])
    fields = []
    while(len(data) > 0):
        f_id = ord(data[0])
        if(f_id < 0x40):
            f_len = lookup[f_id][0]
            d_start = 1  # start of data
        else:
            f_len = ord(data[1])
            d_start = 2  # start of data
        f_value = str_to_int(data[d_start:d_start+f_len])
        fields.append((f_id, f_value))
        data = data[d_start+f_len:]
    return fields

def getFields(data, data_type):
    if data_type in [0x01, 0x02, 0x03]:  # measurement, command
        return lookup_fields(data, PLC_UNITS)
    elif(data_type == 0x06):
        return lookup_fields(data, PLC_ERRORS)
    elif(data_type == 0x04): # parameter type
        start_addr_n_bytes = data[2:]
        param_id = ord(data[0])  #0x61
        start_addr_n_bytes = ConstBitStream(bytes=start_addr_n_bytes).hex
        return [(param_id, start_addr_n_bytes)]
    elif(data_type == 0x08): # rfid type
        bytes_uuid = data[1:]
        param_id = ord(data[0])  #0x3E
        hex_uuid = ConstBitStream(bytes=bytes_uuid).hex
        return [(param_id, hex_uuid)]
    return [('None', 'None')]

